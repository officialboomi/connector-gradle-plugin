// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.gradle

import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

/**
 * Integration specification for the connector plugin. */
class ConnectorPluginISpec extends Specification {

    def project

    def setup() {
        project = ProjectBuilder.builder().build()
        //TODO this does not work in eclipse
        project.getPluginManager().apply("com.boomi.connector")
    }

    def "plugin applies java plugin"() {
        expect:
        project.pluginManager.hasPlugin("java")
    }

    def "plugin applies java-library plugin"() {
        expect:
        project.pluginManager.hasPlugin("java-library")
    }

    def "plugin creates connector extension"() {
        expect:
        project.extensions["connector"] instanceof ConnectorExtension
    }

    def "plugin creates connectorLib configuration"() {
        expect:
        project.configurations["connectorLib"]
    }

    def "plugin adds public sdk repository"() {
        expect:
        project.repositories["boomisdk"].url.toString() == "https://boomisdk.s3.amazonaws.com/releases"
    }

    def "plugin does not add sdk api as an implementation dependency after evaluation"() {
        project.evaluate()

        expect:
        !findDependency("implementation", "connector-sdk-api")
    }

    def "plugin adds sdk api as an incoming implementation dependency"() {
        expect:
        incomingDependency("implementation", "connector-sdk-api")
    }

    def "plugin adds sdk test util as an incoming test implementation dependency"() {
        expect:
        incomingDependency("testImplementation", "connector-sdk-test-util")
    }
    
    def "plugin excludes internal dependencies from connectorLib"() {
        def exclusions = project.configurations["connectorLib"].excludeRules

        expect:
        exclusions.find {(it.group == artifact.group) && (it.module == artifact.module)}

        where:
        artifact << ConnectorArtifact.exclusions()
    }

    def "plugin excludes internal dependencies from connectorHelloWorldLib"() {
        def exclusions = project.configurations["connectorHelloWorldLib"].excludeRules

        expect:
        exclusions.find {(it.group == artifact.group) && (it.module == artifact.module)}

        where:
        artifact << ConnectorArtifact.exclusions()
    }

    def "plugin does not exclude dependencies from connectorSamples"() {
        expect:
        !project.configurations["connectorSamplesLib"].excludeRules
    }

    def "boomi tasks have description"() {
        def tasks = project.tasks.findAll {it.group == "boomi"}

        expect:
        tasks
        tasks.each {
            assert it.description, "missing description"
        }
    }

    def "plugin creates connectorArchive task"() {
        expect:
        with(project.tasks["connectorArchive"]) {
            extension == "zip"
            classifier == "car"
            group == "boomi"
            description == "Assembles the connector archive (car) file."
        }
    }

    def "plugin creates connectorHelloWorldArchive task"() {
        expect:
        withDescription("connectorHelloWorldArchive", "Assembles the hello world connector archive (car) file.")
    }

    def "plugin creates connectorHelloWorldJar task"() {
        expect:
        withDescription("connectorHelloWorldJar",
                "Assembles a jar archive containing the connector hello world classes.")
    }
    
    def "plugin creates connectorSamples task"() {
        expect:
        withDescription("connectorSamples", "Downloads the connector sdk sample source files.")
    }
    
    def "plugin creates connectorConfig task"() {
       expect:
       withDescription("connectorConfig", "Generates the connector runtime configuration file.")
   }
    
    def "plugin creates connectorHelloWorldConfig task"() {
        expect:
        withDescription("connectorHelloWorldConfig", "Generates the connector hello world runtime configuration file.")
    }
    
    def "plugin creates connectorHelloWorldSource task"() {
        expect:
        withDescription("connectorHelloWorldSource", "Generates the connector hello world source files.")
    }
    
    def "plugin creates connectorJsonConfig task"() {
        expect:
        withDescription("connectorJsonConfig", "Generates OpenAPI resource file.")
    }
    
    def "plugin creates connectorDescriptor task"() {
        expect:
        withDescription("connectorDescriptor", "Generates connector-descriptor.xml file from groovy input.")
    }
    
    def "compileConnectorSamplesJava depends on connectorSamples"() {
        expect:
        project.tasks["compileConnectorSamplesJava"].dependsOn.find {it.get().name == "connectorSamples"}
    }

    void withDescription(def task, def desc) {
        with(project.tasks[task]) {
            group == "boomi"
            description == desc
        }
    }

    /**
     * verify a dependency was declared as incoming for the configuration. the incoming dependency will be available 
     * in the base dependency set after interrogating the incoming dependency set */
    void incomingDependency(def configName, def name) {
        assert !findDependency(configName, name)
        assert findIncomingDependency(configName, name)
        assert findDependency(configName, name)
    }

    def find = {def container, def name -> container.find {it.name == name}
    }

    def findDependency = {String configName, def name -> find(project.configurations[configName].dependencies, name)
    }

    def findIncomingDependency = {
        String configName, def name -> find(project.configurations[configName].incoming.dependencies, name)
    }
}
