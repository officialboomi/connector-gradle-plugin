// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.gradle

import com.boomi.connector.gradle.util.TestProps
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

/**
 * Integration spec for the connector extension */
class ConnectorExtensionISpec extends Specification {

    def ext = new ConnectorExtension(ProjectBuilder.builder().build())

    def "class name has no default"() {
        expect:
        ext.hasProperty("className")
        !ext.className
    }

    def "sdk version has default"() {
        expect:
        ext.hasProperty("sdkVersion")
        ext.sdkVersion.get() == TestProps.expectedSdkVersion()
    }

    def "default sdk version can be overridden"() {
        def version = "1.0"

        when:
        ext.sdkVersion = version

        then:
        ext.sdkVersion.get() == version
    }
}
