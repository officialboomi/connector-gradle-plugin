// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.gradle

import com.boomi.connector.gradle.util.ResourceUtil
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Property

/**
 * Simple extension for configuring the connector plugin 
 */
@CompileStatic
class ConnectorExtension {
    
    private static final String DEFAULT_VERSION = ResourceUtil.loadProperties("version.properties").getProperty("sdkVersion")
    
    /**
     * optional sdk version. if not provided the plugin will use a default version 
     */
    final Property<String> sdkVersion
    
    /**
     * connector class name which is required when generating the runtime configuration, ignored otherwise  
     */
    String className 
    
    /*
     * Creates a new extension instance with an optional class name  
     */
    ConnectorExtension(Project project, String className=null) {
        sdkVersion = project.objects.property(String)
        sdkVersion.convention(DEFAULT_VERSION)
        this.className = className
    }

    /**
     * Allows the sdk version to be set without caring that it uses a provider 
     *        
     * @param version the sdk version 
     */
    void setSdkVersion(String version) {
        sdkVersion.set(version)
    }
    
}
