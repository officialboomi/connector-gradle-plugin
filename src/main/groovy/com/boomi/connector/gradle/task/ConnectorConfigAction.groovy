// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.gradle.task

import com.boomi.connector.gradle.ConnectorExtension
import com.boomi.connector.gradle.OpenAPIExtension
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil

import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskAction

/**
 * Task action for generating the connector runtime configuration file 
 */
@CompileStatic
public class ConnectorConfigAction extends ConnectorTaskAction<ConnectorConfigTask> {
    
    private static final String OPENAPIDSL_CONNECTOR_CLASS_NAME = "com.boomi.connector.openapi.dsl.OpenAPIDslConnector"
    private static final String CONFIG_FILENAME = "connector-config.xml"

    /**
     * Creates a new instance with the provided project, source set, and extension
     */
    ConnectorConfigAction(Project project, SourceSet sourceSet, ConnectorExtension ext) {
        this(project, sourceSet, ext, null)
        
    }
    ConnectorConfigAction(Project project, SourceSet sourceSet, ConnectorExtension ext, OpenAPIExtension openApiExt) {
        super(project, '${sourceSetName}Config', ConnectorConfigTask, sourceSet, openApiExt, ext)
    }

    /**
     * The onlyIf condition is purposely set to return true for ConnectorConfigAction task as the task execution doesn't
     * actually depend on the outcome of the predicate, however access to the onlyIf predicate
     * provides a way to evaluate build input during runtime and set the isOpenAPIConnector flag accordingly.
     * */
    @Override
    public void configure(ConnectorConfigTask task) {
        task.outputs.upToDateWhen {false}
        provider(_sourceSet.processResourcesTaskName).get().dependsOn task
        task._ext = _connectorExtension
        task.configFile = "${getOutputMetaInfDirectoryPath()}${CONFIG_FILENAME}" as File
        task.onlyIf {task.isOpenAPIConnector = isOpenAPIConnector(task.name);true}
    }
 
    /**
     * Custom task to generate the config file 
     */
    static class ConnectorConfigTask extends DefaultTask {

        @PackageScope
        ConnectorExtension _ext

        @Input
        boolean isOpenAPIConnector

        @OutputFile
        File configFile

        @TaskAction
        //TODO compile static? template?
        @CompileDynamic
        void generate() {
            def className = _ext.className
           
            if (isOpenAPIConnector) {
                className = OPENAPIDSL_CONNECTOR_CLASS_NAME
            } else if (!className) {
                throw new IllegalArgumentException("connector class name is required")
            }

            Map<String, String> connectorProperties = new HashMap<>()
            connectorProperties.put("sdkApiVersion", _ext.sdkVersion.get())
            connectorProperties.put("strictClassLoading", "true")

            configFile.withWriter {writer ->
                XmlUtil.serialize(new StreamingMarkupBuilder().bind {
                    GenericConnector(connectorProperties) {
                        connectorClassName(className)
                    }
                }, writer)
            }
        }

    }

}