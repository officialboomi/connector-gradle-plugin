/*
 * Copyright (c) 2021 Boomi, Inc.
 */

package com.boomi.connector.gradle.task

import com.boomi.connector.gradle.ConnectorExtension
import com.boomi.connector.gradle.OpenAPIExtension
import com.boomi.connector.gradle.domain.OpenAPIConnectionField
import com.boomi.connector.gradle.validators.OpenAPIGroovyInputValidator
import groovy.transform.CompileDynamic
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskAction

import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.ACCESS
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.ALLOWED_VALUE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.AUTHTYPE_BASIC
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.AUTHTYPE_CUSTOM
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.AUTHTYPE_DIGEST
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.AUTHTYPE_OAUTH2
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.DEFAULT_VALUE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.FIELDID_AUTH_TYPE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.FIELDID_COOKIE_SCOPE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.FIELDID_CUSTOM_AUTH
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.FIELDID_OAUTH2
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.FIELDID_PASSWORD
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.FIELDID_PREEMPTIVE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.FIELDID_PRIVATECERTIFICATE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.FIELDID_PUBLICCERTIFICATE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.FIELDID_URL
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.FIELDID_USERNAME
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.HELP_TEXT
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.LABEL_AUTH_TYPE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.LABEL_COOKIE_SCOPE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.LABEL_CUSTOM_AUTH
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.LABEL_OAUTH2
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.LABEL_PASSWORD
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.LABEL_PREEMPTIVE_AUTH
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.LABEL_PRIVATECERTIFICATE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.LABEL_PUBLICCERTIFICATE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.LABEL_SERVER
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.LABEL_USERNAME
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.NAME
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.PARAMETER
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.TYPE_BOOLEAN
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.TYPE_OAUTH
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.TYPE_PASSWORD
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.TYPE_PRIVATECERTIFICATE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.TYPE_PUBLICCERTIFICATE
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.TYPE_STRING
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.URL_ENDPOINT
import static com.boomi.connector.gradle.util.OpenAPIDescriptorConstants.VALUE

/**
 * Task action for generating the descriptor file from groovy input file.
 * The task looks at configuration input provided under 'openAPIConnector' script block and produces descriptor.xml
 * output file*/
class ConnectorDescriptorAction extends ConnectorTaskAction<ConnectorDescriptorTask> {
    
    private static final String DEFAULT_EXECUTE_OPERATION_TYPE = "EXECUTE"
    private static final String DESCRIPTOR_FILENAME = "connector-descriptor.xml"
    
    /**
     * Creates a new instance
     *
     * @param project the gradle project instance
     * @param sourceSet the primary source set for this task
     * @param openAPIExtension the openAPIConnector extension instance
     * @param connectorExt the connector extension instance
     */
    ConnectorDescriptorAction(Project project, SourceSet sourceSet, OpenAPIExtension openAPIExtension, ConnectorExtension connectorExt) {
        super(project, '${sourceSetName}Descriptor', ConnectorDescriptorTask, sourceSet, openAPIExtension, connectorExt)
    }
    
    /**
     * The task execution depends upon the outcome of onlyIf predicate. The predicate returns true if the application connector has proper build
     * inputs for an OpenAPIConnector
     *
     * @param task local #DefaultTask instance to generate contents of descriptor file
     */
    @Override
    void configure(ConnectorDescriptorTask task) {
        task.outputs.upToDateWhen {false}
        task.openapiExtension = _openAPIExtension
        task.descriptorOutput = "${getOutputMetaInfDirectoryPath()}${DESCRIPTOR_FILENAME}" as File
        provider(_sourceSet.processResourcesTaskName).get().dependsOn task
        task.onlyIf {isOpenAPIConnector(task.name)}
    }
    
    
    
    static class ConnectorDescriptorTask extends DefaultTask {

        @OutputFile
        File descriptorOutput

        protected OpenAPIExtension openapiExtension

        @TaskAction
        @CompileDynamic
        def generate() {
            OpenAPIGroovyInputValidator.validate(openapiExtension)
            descriptorOutput.withWriter {writer -> XmlUtil.serialize(generateDescriptorXML(), writer)
            }
        }
    
        def generateDescriptorXML() {
            def xml = new StreamingMarkupBuilder().bind {builder ->
                GenericConnectorDescriptor(requireConnectionForBrowse: true) {
                    buildConnectionFields(builder)
                    buildOperations(builder)
                }
            }
            xml
        }
    
        protected def buildOperations(builder) {
            openapiExtension.operations.each {
                builder.operation(types: DEFAULT_EXECUTE_OPERATION_TYPE, supportsBrowse: true,
                        customTypeId: it.name.replaceAll(/\s*/, ""), customTypeLabel: it.name)
            }
        }
    
        protected def buildConnectionFields(builder) {
            buildServerField(builder)
            buildCookieScopeField(builder)
            buildPrivateCertField(builder)
            buildPublicCertField(builder)
            buildAuthenticationTypeField(builder)
        }
    
       private void buildServerField(builder) {
            buildSimpleConnectionField(FIELDID_URL, LABEL_SERVER, TYPE_STRING, openapiExtension.server, builder)
        }
    
        private void buildCookieScopeField(builder) {
            if (openapiExtension.cookieScope instanceof String) {
                def allowedValuesEntry = [openapiExtension.cookieScope]
                OpenAPIConnectionField cookieScope = new OpenAPIConnectionField(openapiExtension.cookieScope)
                cookieScope.label(LABEL_COOKIE_SCOPE)
                buildConnectionField(FIELDID_COOKIE_SCOPE, TYPE_STRING, allowedValuesEntry, cookieScope, builder)
            } else if (openapiExtension?.cookieScope instanceof OpenAPIConnectionField) {
                def cookieLabel = (openapiExtension.cookieScope?.label) ? openapiExtension.cookieScope.label : LABEL_COOKIE_SCOPE
                openapiExtension.cookieScope.label(cookieLabel)
                buildConnectionField(FIELDID_COOKIE_SCOPE, TYPE_STRING, openapiExtension.cookieScope?.includedValues,
                        openapiExtension.cookieScope, builder)
            }
        }
    
        private void buildAuthenticationTypeField(builder) {
            if (openapiExtension.authType instanceof String) {
                buildAuthTypeFieldDefinedAsString(builder)
            } else if (openapiExtension?.authType instanceof OpenAPIConnectionField) {
                buildAuthTypeFieldDefinedAsClosure(builder)
            }
        }
    
        private void buildPrivateCertField(builder) {
            if (openapiExtension?.includePrivateCert instanceof Boolean && openapiExtension?.includePrivateCert) {
                buildSimpleConnectionField(FIELDID_PRIVATECERTIFICATE, LABEL_PRIVATECERTIFICATE,
                        TYPE_PRIVATECERTIFICATE, null, builder)
            } else if (openapiExtension?.includePrivateCert instanceof OpenAPIConnectionField) {
                def privateCertLabel = (openapiExtension.includePrivateCert?.label) ? openapiExtension.includePrivateCert.label :
                        LABEL_PRIVATECERTIFICATE
                openapiExtension.includePrivateCert.label(privateCertLabel)
                buildConnectionField(FIELDID_PRIVATECERTIFICATE, TYPE_PRIVATECERTIFICATE, null,
                        openapiExtension.includePrivateCert, builder)
            }
        }
    
        private void buildPublicCertField(builder) {
            if (openapiExtension?.includePublicCert instanceof Boolean && openapiExtension?.includePublicCert) {
                buildSimpleConnectionField(FIELDID_PUBLICCERTIFICATE, LABEL_PUBLICCERTIFICATE, TYPE_PUBLICCERTIFICATE,
                        null, builder)
            } else if (openapiExtension?.includePublicCert instanceof OpenAPIConnectionField) {
                def publicCertLabel = (openapiExtension.includePublicCert.label) ? openapiExtension.includePublicCert.label : LABEL_PUBLICCERTIFICATE
                openapiExtension.includePublicCert.label(publicCertLabel)
                buildConnectionField(FIELDID_PUBLICCERTIFICATE, TYPE_PUBLICCERTIFICATE, null,
                        openapiExtension.includePublicCert, builder)
            }
        }
    
        private void buildAuthTypeFieldDefinedAsString(builder) {
            def allowedValuesEntry = [openapiExtension.authType]
            OpenAPIConnectionField authType = new OpenAPIConnectionField(openapiExtension.authType)
            authType.label(LABEL_AUTH_TYPE)
            buildAuthTypeField(builder, allowedValuesEntry, authType)
            //Building other conditional fields based on the auth type
            switch (openapiExtension.authType.toUpperCase()) {
                case AUTHTYPE_BASIC:
                    buildUsernamePasswordFields(builder, Collections.emptyList())
                    buildPreemptiveAuthField(builder, Collections.emptyList())
                    break
                case AUTHTYPE_CUSTOM:
                    buildCustomAuthField(builder, Collections.emptyList())
                    break
                case AUTHTYPE_DIGEST:
                    buildUsernamePasswordFields(builder, Collections.emptyList())
                    break
                case AUTHTYPE_OAUTH2:
                    buildOAuth2Field(builder)
                    buildPreemptiveAuthField(builder, Collections.emptyList())
                    break
                default:
                    break
            }
        }
    
        private void buildAuthTypeFieldDefinedAsClosure(builder) {
            def authTypeLabel = (openapiExtension?.authType?.label) ? openapiExtension.authType.label : LABEL_AUTH_TYPE
            openapiExtension?.authType?.label(authTypeLabel)
            buildAuthTypeField(builder, openapiExtension?.authType?.includedValues,
                    openapiExtension?.authType)
            //building dependent fields. includedValues also contains defaultValue entry if available
            if (openapiExtension?.authType?.defaultValue && !openapiExtension?.authType?.includedValues) {
                openapiExtension?.authType?.includedValues = [openapiExtension.authType.defaultValue]
            }
            def visibilityValueConditions = []
            if (openapiExtension?.authType?.includedValues?.contains(AUTHTYPE_BASIC)) {
                visibilityValueConditions.add(AUTHTYPE_BASIC)
            }
            if (openapiExtension?.authType?.includedValues?.contains(AUTHTYPE_DIGEST)) {
                visibilityValueConditions.add(AUTHTYPE_DIGEST)
            }
            if (visibilityValueConditions.size() >= 1) {
                buildUsernamePasswordFields(builder, visibilityValueConditions)
                // removing DIGEST alone and keeping BASIC if in
                visibilityValueConditions.remove(AUTHTYPE_DIGEST)
            }
            if (openapiExtension?.authType?.includedValues?.contains(AUTHTYPE_OAUTH2)) {
                visibilityValueConditions.add(AUTHTYPE_OAUTH2)
                buildOAuth2Field(builder)
            }
            if (visibilityValueConditions?.contains(AUTHTYPE_BASIC) || visibilityValueConditions?.contains(AUTHTYPE_OAUTH2)) {
                buildPreemptiveAuthField(builder, visibilityValueConditions)
            }
            if (openapiExtension?.authType?.includedValues?.contains(AUTHTYPE_CUSTOM)) {
                buildCustomAuthField(builder, [AUTHTYPE_CUSTOM])
            }
        }
    
        /**
         * Helper methods to build different connection fields
         * */
        private void buildAuthTypeField(builder, List includedValues, OpenAPIConnectionField fieldType) {
            buildConnectionField(FIELDID_AUTH_TYPE, TYPE_STRING, includedValues, fieldType, builder)
        }
    
        private void buildCustomAuthField(builder, List visibilityValueConditions) {
            buildSimpleConnectionField(FIELDID_CUSTOM_AUTH, LABEL_CUSTOM_AUTH, TYPE_PASSWORD, null, builder, visibilityValueConditions)
        }
    
        private void buildPreemptiveAuthField(builder, List visibilityValueConditions) {
            buildSimpleConnectionField(FIELDID_PREEMPTIVE, LABEL_PREEMPTIVE_AUTH, TYPE_BOOLEAN, "false", builder, visibilityValueConditions)
        }
    
        private void buildOAuth2Field(builder) {
            if (openapiExtension?.oAuth2Config) {
                buildOAuth2Config(builder)
            } else {
                buildSimpleConnectionField(FIELDID_OAUTH2, LABEL_OAUTH2, TYPE_OAUTH, null, builder)
            }
        }
    
        /**
         * Builder method to construct different OAuth2 sub-fields based on the groovy input.
         *
         */
        private void buildOAuth2Config(builder){
            builder.field(id: FIELDID_OAUTH2, label: LABEL_OAUTH2, type: TYPE_OAUTH) {
                "${HELP_TEXT}"("OAuth 2.0 Options")
                oauth2FieldConfig{
                    if (openapiExtension.oAuth2Config?.authorizationTokenEndpoint) {
                        // doesn't accept helpText
                        authorizationTokenEndpoint {
                            "${URL_ENDPOINT}"("${ACCESS}": openapiExtension.oAuth2Config.authorizationTokenEndpoint?.access) {
                                "${DEFAULT_VALUE}"(openapiExtension.oAuth2Config.authorizationTokenEndpoint?.defaultValue)
                            }
            
                        }
                    }
                    if (openapiExtension.oAuth2Config?.authorizationParameters) {
                        authorizationParameters("${ACCESS}": openapiExtension.oAuth2Config.authorizationParameters?.access) {
                            openapiExtension.oAuth2Config.authorizationParameters?.keyValuePairs?.collect {k, v ->
                                "${PARAMETER}"("${NAME}": k) {
                                    "${VALUE}" {"${DEFAULT_VALUE}"(v)}
                                }
                
                            }
                        }
                    }
                    if (openapiExtension.oAuth2Config?.accessTokenEndpoint) {
                        // doesn't accept helpText
                        accessTokenEndpoint {
                            "${URL_ENDPOINT}"("${ACCESS}": openapiExtension.oAuth2Config.accessTokenEndpoint?.access) {
                                "${DEFAULT_VALUE}"(openapiExtension.oAuth2Config.accessTokenEndpoint?.defaultValue)
                            }
                        }
                    }
                    if (openapiExtension.oAuth2Config?.accessTokenParameters) {
                        accessTokenParameters("${ACCESS}": openapiExtension.oAuth2Config.accessTokenParameters?.access) {
                            openapiExtension.oAuth2Config.accessTokenParameters?.keyValuePairs?.collect {k, v ->
                                "${PARAMETER}"("${NAME}": k) {
                                    "${VALUE}" {"${DEFAULT_VALUE}"(v)}
                                }
                
                            }
                        }
                    }
                    if (openapiExtension.oAuth2Config?.scope) {
                        scope("${ACCESS}": openapiExtension.oAuth2Config.scope?.access) {
                            if (openapiExtension.oAuth2Config.scope?.helpText) {
                                "${HELP_TEXT}"(openapiExtension.oAuth2Config.scope?.helpText)
                            }
                            "${DEFAULT_VALUE}"(openapiExtension.oAuth2Config.scope?.defaultValue)
                        }
                    }
                    if (openapiExtension.oAuth2Config?.grantType) {
                        grantType("${ACCESS}": openapiExtension.oAuth2Config.grantType?.access) {
                            if (openapiExtension.oAuth2Config.grantType?.helpText) {
                                "${HELP_TEXT}"(openapiExtension.oAuth2Config.grantType?.helpText)
                            }
                            "${DEFAULT_VALUE}"(openapiExtension.oAuth2Config.grantType?.defaultValue)
    
                            List grantTypeValuesList = (openapiExtension.oAuth2Config?.grantType?.hasProperty('includedValues') && openapiExtension
                                    .oAuth2Config.grantType?.includedValues) ? openapiExtension.oAuth2Config.grantType?.includedValues :
                                    [openapiExtension.oAuth2Config.grantType?.defaultValue]
                            if (!grantTypeValuesList.contains(openapiExtension.oAuth2Config.grantType?.defaultValue)) {
                                grantTypeValuesList.add(openapiExtension.oAuth2Config.grantType?.defaultValue)
                            }
                            grantTypeValuesList.collect {option ->
                                "${ALLOWED_VALUE}" {
                                    "${VALUE}"(option)
                                }
                            }
                        }
                    }
                    if (openapiExtension.oAuth2Config?.jwtParameters) {
                        jwtParameters {
                            if (openapiExtension.oAuth2Config.jwtParameters?.signatureAlgorithms) {
                                signatureAlgorithms("${ACCESS}": openapiExtension.oAuth2Config.jwtParameters.signatureAlgorithms?.access) {
                                    if (openapiExtension.oAuth2Config.jwtParameters.signatureAlgorithms?.helpText) {
                                        "${HELP_TEXT}"(openapiExtension.oAuth2Config.jwtParameters.signatureAlgorithms?.helpText)
                                    }
                                    "${DEFAULT_VALUE}"(openapiExtension.oAuth2Config.jwtParameters.signatureAlgorithms?.defaultValue)
    
                                    List signAlgoValuesList = openapiExtension.oAuth2Config.jwtParameters.signatureAlgorithms?.hasProperty(
                                            'includedValues') && openapiExtension.oAuth2Config.jwtParameters.signatureAlgorithms?.includedValues ?
                                            openapiExtension.oAuth2Config.jwtParameters.signatureAlgorithms?.includedValues :
                                                    [openapiExtension.oAuth2Config.jwtParameters.signatureAlgorithms?.defaultValue]
                                    if (!signAlgoValuesList.contains(openapiExtension.oAuth2Config.jwtParameters.signatureAlgorithms?.defaultValue)) {
                                        signAlgoValuesList.add(openapiExtension.oAuth2Config.jwtParameters.signatureAlgorithms?.defaultValue)
                                    }
                                    signAlgoValuesList.collect {option ->
                                        "${ALLOWED_VALUE}" {
                                            "${VALUE}"(option)
                                        }
                                    }
                                }
                            }
                            if (openapiExtension.oAuth2Config.jwtParameters?.issuer) {
                                issuer("${ACCESS}": openapiExtension.oAuth2Config.jwtParameters.issuer?.access) {
                                    if (openapiExtension.oAuth2Config.jwtParameters.issuer?.helpText) {
                                        "${HELP_TEXT}"(openapiExtension.oAuth2Config.jwtParameters.issuer?.helpText)
                                    }
                                    "${DEFAULT_VALUE}"(openapiExtension.oAuth2Config.jwtParameters.issuer?.defaultValue)
                                }
                            }
                            if (openapiExtension.oAuth2Config.jwtParameters?.subject) {
                                subject("${ACCESS}": openapiExtension.oAuth2Config.jwtParameters.subject?.access) {
                                    if (openapiExtension.oAuth2Config.jwtParameters.subject?.helpText) {
                                        "${HELP_TEXT}"(openapiExtension.oAuth2Config.jwtParameters.subject?.helpText)
                                    }
                                    "${DEFAULT_VALUE}"(openapiExtension.oAuth2Config.jwtParameters.subject?.defaultValue)
                                }
                            }
                            if (openapiExtension.oAuth2Config.jwtParameters?.audience) {
                                audience("${ACCESS}": openapiExtension.oAuth2Config.jwtParameters.audience?.access) {
                                    if (openapiExtension.oAuth2Config.jwtParameters.audience?.helpText) {
                                        "${HELP_TEXT}"(openapiExtension.oAuth2Config.jwtParameters.audience?.helpText)
                                    }
                                    "${DEFAULT_VALUE}"(openapiExtension.oAuth2Config.jwtParameters.audience?.defaultValue)
                                }
                            }
                            if (openapiExtension.oAuth2Config.jwtParameters?.expiration) {
                                expiration("${ACCESS}": openapiExtension.oAuth2Config.jwtParameters.expiration?.access) {
                                    if (openapiExtension.oAuth2Config.jwtParameters.expiration?.helpText) {
                                        "${HELP_TEXT}"(openapiExtension.oAuth2Config.jwtParameters.expiration?.helpText)
                                    }
                                    "${DEFAULT_VALUE}"(openapiExtension.oAuth2Config.jwtParameters.expiration?.defaultValue)
                                }
                            }
                            if (openapiExtension.oAuth2Config.jwtParameters?.signatureKey) {
                                signatureKey("${ACCESS}": openapiExtension.oAuth2Config.jwtParameters.signatureKey?.access) {
                                    if (openapiExtension.oAuth2Config.jwtParameters.signatureKey?.helpText) {
                                        "${HELP_TEXT}"(openapiExtension.oAuth2Config.jwtParameters.signatureKey?.helpText)
                                    }
                                }
                            }
                            if (openapiExtension.oAuth2Config.jwtParameters?.extendedClaims) {
                                extendedClaims("${ACCESS}": openapiExtension.oAuth2Config.jwtParameters.extendedClaims?.access) {
                                    openapiExtension.oAuth2Config.jwtParameters.extendedClaims?.keyValuePairs?.collect {k, v ->
                                        claim("${NAME}": k) {
                                            "${VALUE}" {"${DEFAULT_VALUE}"(v)}
                                        }
                        
                                    }
                                }
                            }
                            //idGeneratorMethods always set to 'NONE'
                            idGeneratorMethods("${ACCESS}": 'hidden') {
                                "${ALLOWED_VALUE}" {"${VALUE}"("NONE")}
                            }
                        }
                    }
                }
            }
        }
    
        private void buildUsernamePasswordFields(builder, List visibilityValueConditions) {
            buildSimpleConnectionField(FIELDID_USERNAME, LABEL_USERNAME, TYPE_STRING, null, builder, visibilityValueConditions)
            buildSimpleConnectionField(FIELDID_PASSWORD, LABEL_PASSWORD, TYPE_PASSWORD, null, builder, visibilityValueConditions)
        }
        /**
         * Helper method to construct an input field with dropdown options. Also sets defaultValue of the field if provided
         * */
        private void buildConnectionField(String fieldId, String type, List validValues, OpenAPIConnectionField connectionField,
                builder) {
            if (!fieldId) {
                throw new IllegalArgumentException("'id' can't be null. Please check your input.")
            }
            if (!type) {
                throw new IllegalArgumentException("'type' can't be null. Please check your input.")
            }
            builder.field(id: fieldId, label: connectionField.label, type: type) {
                if (connectionField?.helpText) {
                    "${HELP_TEXT}"(connectionField?.helpText)
                }
                if (connectionField?.defaultValue) {
                    "${DEFAULT_VALUE}"(connectionField?.defaultValue?.toUpperCase())
                    if (!validValues) {
                        validValues = []
                    }
                    if (!validValues.contains(connectionField?.defaultValue)) {
                        validValues.add(connectionField?.defaultValue?.toUpperCase())
                    }
                }
    
                if (validValues) {
                    validValues.collect {option ->
                            allowedValue(label: option) {
                                value(option)
                        }
                    }
                }
            }
        }

        /**
         * Helper method to construct a simple input field with defaultValue if provided.
         * */
        private void buildSimpleConnectionField(String fieldId, String labelText, String type, String defaultInput, builder) {
            if (!fieldId) {
                throw new IllegalArgumentException("'id' can't be null. Please check your input.")
            }
            if (!type) {
                throw new IllegalArgumentException("'type' can't be null. Please check your input.")
            }
            builder.field(id: fieldId, label: labelText, type: type) {
                if (defaultInput) {
                    defaultValue(defaultInput)
                }
            }
            builder
        }
    
        /**
         * Helper method to construct a simple input field with defaultValue if provided. Also lets build conditional visibility based on value(s)
         * of "auth" field.
         * */
        private void buildSimpleConnectionField(String fieldId, String labelText, String type, String defaultInput, builder, List visibilityValueConditions) {
            if (!fieldId) {
                throw new IllegalArgumentException("'id' can't be null. Please check your input.")
            }
            if (!type) {
                throw new IllegalArgumentException("'type' can't be null. Please check your input.")
            }
            builder.field(id: fieldId, label: labelText, type: type) {
                if (defaultInput) {
                    defaultValue(defaultInput)
                }
                if (visibilityValueConditions) {
                    visibilityCondition {
                        valueCondition(fieldId: "auth") {
                            visibilityValueConditions.each {conditionItem -> value(conditionItem)
                            }
                        }
                    }
                }
            }
            builder
        }
    }
}