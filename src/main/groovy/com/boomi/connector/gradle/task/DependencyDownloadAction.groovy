// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.gradle.task

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.SourceSet

/**
 * Task action for downloading a dependency and copying the contents to a "generated" source directory. Transitive 
 * dependencies will be disabled by the task but it is the client responsibility to ensure that only 1 dependency 
 * is added to the configuration. Otherwise, the task will fail when resolving the dependency.   
 */
@CompileStatic

public class DependencyDownloadAction extends ConnectorTaskAction<Copy> {

    private final Configuration _configuration

    /**
     * Creates a new instance 
     * 
     * @param project the gradle project instance
     * @param sourceSet the source set for this task 
     * @param the name of the configuration with containing the dependency 
     */
    DependencyDownloadAction(Project project, SourceSet sourceSet, String configurationName) {
        super(project, '${sourceSetName}', Copy, sourceSet)
        _configuration = extendConfiguration(configurationName).get().setTransitive(false)
    }

    @Override
    void configure(Copy task) {
        task.from _project.zipTree(_configuration.singleFile)
        task.into getSourceDirectory()
    }
}
