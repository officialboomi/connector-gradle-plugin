// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.gradle.task

import com.boomi.connector.gradle.util.ResourceUtil
import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.OutputFiles
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskAction
import org.gradle.internal.service.scopes.Scopes

/**
 * Task action for "generating" the connector hello world source. The source is actaully available 
 * as plugin resources so the task merely copies the source to a generated source directory. The actual 
 * task name will be derived from the source set. 
 */
@CompileStatic
public class HelloWorldSourceAction extends ConnectorTaskAction<CopyResourceTask> {

    private static final String PACKAGE = "com.boomi.connector.sample"
    private static final String PACKAGE_DIR = PACKAGE.replaceAll(/\./, /\//)
    private static final String CLASS_NAME = "HelloWorldConnector";

    static final String CONNECTOR_CLASS_NAME = "${PACKAGE}.${CLASS_NAME}"

    private final File _sourceDir
    private final File _resourceDir 

    /**
     * Creates a new instance 
     */
    public HelloWorldSourceAction(Project project, SourceSet sourceSet) {
        super(project, '${sourceSetName}Source', CopyResourceTask, sourceSet)
        _sourceDir = "${getSourceDirectory()}/${PACKAGE_DIR}" as File
        _resourceDir = getResourceDirectory()
    }

    @Override
    public void configure(CopyResourceTask task) {     
        task.put("$_sourceDir/${CLASS_NAME}.java" as File)
        task.put("$_sourceDir/HelloWorldOperation.java" as File)
        task.put("HelloWorld-descriptor.xml", "$_resourceDir/META-INF/connector-descriptor.xml" as File)
    }

    /**
     * Simple task to copy resources to the source directory. This is not intended for "large" files.  
     */
    static class CopyResourceTask extends DefaultTask {

        @OutputFiles
        Map<String, File> output = [:]       
        
        @TaskAction
        void copy() {
            //TODO skip existing? 
            output.each {
                it.value.withOutputStream { output ->
                    ResourceUtil.getResource(it.key).withCloseable { input ->
                        output.write(input.bytes)
                    }
                }
            }
        }

        void put(File file) {
            put(file.name, file)
        }
        
        void put(String name, File file) {
            output[name] = file
        }

    }
}
