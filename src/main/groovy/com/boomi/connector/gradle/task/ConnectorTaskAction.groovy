// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.gradle.task

import com.boomi.connector.gradle.ConnectorArtifact
import com.boomi.connector.gradle.ConnectorExtension
import com.boomi.connector.gradle.OpenAPIExtension
import com.boomi.connector.gradle.util.ResourceUtil
import groovy.text.SimpleTemplateEngine
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.NamedDomainObjectProvider
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.Configuration
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskProvider

/**
 * Base class for defining connector actions and tasks. Each action implementation is required to provide the task type 
 * which will be registered with the project via #registerTask
 */
@CompileStatic

abstract class ConnectorTaskAction<T extends Task> implements Action<T> {

    static final String GROUP = "boomi"

    private static final Logger LOG = Logging.getLogger(ConnectorTaskAction)
    private static final Properties TASK_CONFIG = ResourceUtil.loadProperties("task-config.properties")

    private final Class<T> _taskType
    private final String _taskName

    protected final SourceSet _sourceSet
    protected final Project _project
    
    protected final OpenAPIExtension _openAPIExtension
    protected final ConnectorExtension _connectorExtension

    /**
     * Creates a new instance 
     * 
     * @param project the gradle project instance 
     * @param taskNameTemplate the template for the task name  
     * @param taskType the type of the task to create 
     * @param sourceSet the primary source set for this task  
     */
    public ConnectorTaskAction(Project project, String taskNameTemplate, Class<T> taskType, SourceSet sourceSet) {
        this(project,taskNameTemplate, taskType, sourceSet, null)
    }
    
    /**
     * Creates a new instance
     *
     * @param project the gradle project instance
     * @param taskNameTemplate the template for the task name
     * @param taskType the type of the task to create
     * @param sourceSet the primary source set for this task
     * @param openAPIExtension the openAPIConnector extension instance
     */
    public ConnectorTaskAction(Project project, String taskNameTemplate, Class<T> taskType, SourceSet sourceSet, OpenAPIExtension openAPIExtension ) {
        this(project,taskNameTemplate, taskType, sourceSet, openAPIExtension, null)
    }
    
    /**
     * Creates a new instance
     *
     * @param project
     * @param taskNameTemplate
     * @param taskType
     * @param sourceSet
     * @param openAPIExtension the openAPIConnector extension instance
     * @param connectorExt the connector extension instance
     */
    public ConnectorTaskAction(Project project, String taskNameTemplate, Class<T> taskType, SourceSet sourceSet,OpenAPIExtension openAPIExtension, ConnectorExtension connectorExtension) {
        _project = project
        _taskName = makeName(sourceSet, taskNameTemplate)
        _taskType = taskType
        _sourceSet = sourceSet
        _openAPIExtension = openAPIExtension
        _connectorExtension = connectorExtension
    }
    /**
     * Registers the task with the project 
     * 
     * @return task provider for the new task 
     */
    public TaskProvider<T> registerTask() {
        _project.tasks.register(_taskName, _taskType, this)
    }

    /**
     * Perform common configuration for all boomi tasks and invokes #configure for task specific configuration 
     */
    @Override
    public final void execute(T task) {
        task.setGroup GROUP
        task.setDescription getDescription(_taskName)
        configure(task)
    }

    /**
     * Configure the task. Implementations should perform any custom configuration here. 
     * 
     * @param task the registered task 
     */
    abstract void configure(T task);

    /**
     * Convenience method for obtaining a task provider from the project 
     * 
     * @param name the name of the task 
     * @return provider for the task 
     */
    protected TaskProvider<? extends Task> provider(String name) {
        _project.tasks.named(name)
    }
    
    /**
     * Convenience method for obtaining the first source directory from the source set.  
     * 
     * @return the source directory 
     */
    protected File getSourceDirectory() {
        _sourceSet.java.srcDirs[0]
    }

    /**
     * Convenience method for obtaining the first resource directory from the source set.
     *
     * @return the resource directory
     */
    protected File getResourceDirectory() {
        _sourceSet.resources.srcDirs[0]
    }

    /**
     * Convenience method for obtaining path of the META-INF directory from the source set.
     * @return
     */
    protected String getOutputMetaInfDirectoryPath(){
        "${_sourceSet.output.resourcesDir}/META-INF/"
    }

    /**
     * Convenience method to extend an existing configuration. The name of the new configuration is derived from the 
     * ${sourceSetName}Lib template. This method is intentionally final as it's used in some constructors. 
     *   
     * @param parentConfigurationName the name of the configuration to extend 
     * @param exclusions artifacts to exclude from this configuration (empty by default) 
     * @return provider for the new configuration 
     */
    protected final NamedDomainObjectProvider<Configuration> extendConfiguration(String parentConfigurationName, Iterable<Map<String, String>> exclusions=[]) {
        def name = makeName(_sourceSet, '${sourceSetName}Lib')
        _project.configurations.register(name, { Configuration conf ->
            conf.extendsFrom _project.configurations[parentConfigurationName]
            exclusions.each { conf.exclude it }
        })
    }

    /**
     * Convenience method to derive a name from a source set and a name template. sourceSetName is the only supported bind variable. 
     * @param sourceSet the source set 
     * @param template the name template 
     * @return the derived name 
     */
    protected static String makeName(SourceSet sourceSet, String template) {
        def binding = [sourceSetName: getSourceSetName(sourceSet)]
        new SimpleTemplateEngine().createTemplate(template).make(binding).toString().uncapitalize()
    }
    
    /**
     * Convenience method to retrieve name of a sourceset
     * @param sourceSet
     * @return Returns "Connector" if main sourceset, otherwise returns the registered name
     */
    private static String getSourceSetName (SourceSet sourceSet){
        isMain(sourceSet) ? "connector" : sourceSet.name
    }

    /**
     * Convenience method to determine if a source set is the main source based on name 
     * 
     * @param sourceSet the source set 
     * @return true if the source set name is "main", false otherwise 
     */
    protected static boolean isMain(SourceSet sourceSet) {
        (SourceSet.MAIN_SOURCE_SET_NAME == sourceSet.name)
    }

    /**
     * Get the task description from the task config properties 
     * 
     * @param taskName the name of the task 
     * @return the task description (if available) 
     */
    private static String getDescription(String taskName) {
        TASK_CONFIG.getProperty("${taskName}.description")
    }
    
    /**
     * Adds a dependency on a connector artifact to the specified configuration. Currently only supports sdk
     * versioned artifacts. Picks the version from 'connector' extension if provided, else uses the DEFAULT_VERSION
     *
     * @param configurationName the name of the configuration where the dependency will be added
     * @param artifact the artifact
     * @param classifier the optional artifact classifier
     * */
    protected void addDependency(String configurationName, ConnectorArtifact artifact, String classifier = null) {
        _project.configurations[configurationName].withDependencies { deps ->
            def dep = artifact.dependency(_connectorExtension.sdkVersion.get())
            deps.add _project.dependencies.create(classifier ? "$dep:$classifier" : dep)
        }
    }
    
    /**
     *  Evaluates if the application connector qualifies as an OpenAPIConnector or not. If it does, adds #ConnectorArtifact.SDK_OPEN_API artifact
     *  as a dependency automatically. Users wouldn't have to make an explicit inclusion in their build files. Also adds Maven Central Repository
     *  to the project used to resolve some transitive dependencies of SDK_OPEN_API artifact.
     *
     * @param taskName Name of the task which getting executed
     * @return true if the application connector executing the build/task has valid inputs for an OpenAPIConnector
     */
    protected boolean isOpenAPIConnector(String taskName) {
        boolean isOpenAPIConnector = false
        if (isOpenAPIExtensionValid(_openAPIExtension)) {
            isOpenAPIConnector = true
        } else if(taskName in ["connectorJsonConfig", "connectorDescriptor"]) {
            logTaskSkippedWarning(taskName)
        }
        return isOpenAPIConnector
    }
    
    /**
     * Helper method to determine if the connector is an OpenAPIConnector or not.
     *
     * @param openAPIExtension Extension instance
     * @return true if the build inputs use openAPIConnector extension with atleast one of the required fields.
     */
    private static boolean isOpenAPIExtensionValid(OpenAPIExtension openAPIExtension) {
        if (!(openAPIExtension?.openApiSpecification || openAPIExtension?.authType || openAPIExtension?.operations)) {
            return false
           }
        true
    }
    
    /**
     * Helper method to add a warning and provide additional information
     *
     * @param taskName
     */
    private static void logTaskSkippedWarning(String taskName) {
        LOG.debug("${taskName} has been skipped. To run this task, you need to use 'openAPIConnector' extension with required fields information. " +
                "Please refer to README for more details.")
    }
    
    /**
     * Adds Maven Central Repository to the project
     *
     * */
    private void addMavenCentralRepo() {
        _project.repositories.mavenCentral()
    }
    
}
