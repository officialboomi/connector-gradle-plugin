// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.gradle.task

import org.gradle.api.NamedDomainObjectProvider
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.file.CopySpec
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.bundling.Zip

import com.boomi.connector.gradle.ConnectorArtifact

import groovy.transform.CompileStatic

/**
 * Task action for generating connector archives. This task extends {@link Jar} instead of {@link Zip} to get the manifest functionality for free.
 * 
 */
@CompileStatic
class ConnectorArchiveAction extends ConnectorTaskAction<Jar> {

    private final SourceSet _sourceSet
    private final NamedDomainObjectProvider<Configuration> _connectorLib

    ConnectorArchiveAction(Project project, SourceSet sourceSet, String parentConfigurationName) {
        super(project, '${sourceSetName}Archive', Jar, sourceSet)
        _sourceSet = sourceSet
        _connectorLib = extendConfiguration(parentConfigurationName, ConnectorArtifact.exclusions())
    }

    @Override
    void configure(Jar task) {
        if (!isMain(_sourceSet)) {
            task.archiveBaseName.set _sourceSet.name
        }
        task.archiveClassifier.set "car"
        task.archiveExtension.set "zip"
        task.destinationDirectory.set _project.file("$_project.buildDir/distributions")
        task.into "lib", { CopySpec spec ->
            spec.from provider(_sourceSet.jarTaskName)
            spec.from _connectorLib
        }
        task.from provider(_sourceSet.processResourcesTaskName), { CopySpec spec ->
            spec.include "META-INF/**/*"
        }
        task.doFirst {
            task.manifest.attributes.putAll([
                "connector-name": _project.name,
                "connector-version": _project.version,
                "connector-description": _project.description
            ].findAll { it.value && !task.manifest.attributes[it.key]})
        }
    }
}
