/*
 * Copyright (c) 2021 Boomi, Inc.
 */

package com.boomi.connector.gradle.task

import com.boomi.connector.gradle.ConnectorExtension
import com.boomi.connector.gradle.OpenAPIExtension
import groovy.json.JsonBuilder
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskAction

/**
 * Task action for generating the OpenAPI resource configuration file.
 * The task looks at configuration input provided under 'openAPIConnector' script block and produces json output file*/
@CompileStatic
public class ConnectorJsonConfigAction extends ConnectorTaskAction<ConnectorJsonConfigTask> {

    private static final String JSON_CONFIG_FILENAME = "openapi-config.json"
    
    /**
     * Creates a new instance
     *
     * @param project the gradle project instance
     * @param sourceSet the primary source set for this task
     * @param openAPIExtension the OpenAPI extension
     * @param connectorExt the connector extension instance
     */
    public ConnectorJsonConfigAction(Project project, SourceSet sourceSet, OpenAPIExtension openAPIExtension, ConnectorExtension connectorExt) {
        super(project, '${sourceSetName}JsonConfig', ConnectorJsonConfigTask, sourceSet, openAPIExtension, connectorExt)
    }
    
    /**
     * The task execution depends upon the outcome of onlyIf predicate. The predicate returns true if the application connector has proper build
     * inputs for an OpenAPIConnector
     *
     * @param task local #DefaultTask instance to generate contents of descriptor file
     */
    @Override
    public void configure(ConnectorJsonConfigTask task) {
        task.outputs.upToDateWhen {false}
        task.openapiExtension = _openAPIExtension
        task.openAPIConfigFile = "${getOutputMetaInfDirectoryPath()}${JSON_CONFIG_FILENAME}" as File
        provider(_sourceSet.processResourcesTaskName).get().dependsOn task
        task.onlyIf {isOpenAPIConnector(task.name)}
    }

    static class ConnectorJsonConfigTask extends DefaultTask {

        @OutputFile
        File openAPIConfigFile

        protected OpenAPIExtension openapiExtension
       
        @TaskAction
        @CompileDynamic
        def generate() {
            validateInputs()
            generateResourceConfigJson()
        }
    
        private void validateInputs() {
            if (!openapiExtension.openApiSpecification) {
                throw new IllegalArgumentException(
                        "'openApiSpecification' can't be empty. Please make sure you include it in the configuration")
            }
            if (!openapiExtension.operations) {
                throw new IllegalArgumentException(
                        "'operations' can't be empty. Please make sure you define atleast one operation as part of "
                                + "the configuration")
            }
        }

        private void generateResourceConfigJson() {
            def resourceFileContent = [:]
            resourceFileContent.operations = openapiExtension.operations
            resourceFileContent.spec = openapiExtension.openApiSpecification
            resourceFileContent.baseUrl = openapiExtension.server
            resourceFileContent.authType = openapiExtension.authType
            openAPIConfigFile.withWriter { it << new JsonBuilder(resourceFileContent).toPrettyString()}
        }
    }
}
