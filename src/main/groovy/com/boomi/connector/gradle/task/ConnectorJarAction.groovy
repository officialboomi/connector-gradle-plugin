// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.gradle.task

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.bundling.Jar

/**
 * Task action for generating a connector jar 
 */
@CompileStatic

public class ConnectorJarAction extends ConnectorTaskAction<Jar> {
    
    /**
     * Creates a new instance with the provided project and source set 
     */
    ConnectorJarAction(Project project, SourceSet sourceSet) {
        super(project, sourceSet.jarTaskName, Jar, sourceSet)
    }
    
    @Override
    public void configure(Jar task) {
        task.from _sourceSet.output 
        task.archiveBaseName.set _sourceSet.name 
    }
    
}
