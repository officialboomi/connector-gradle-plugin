// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.gradle

import com.boomi.connector.gradle.factory.CoreTaskFactory
import com.boomi.connector.gradle.factory.HelloWorldTaskFactory
import com.boomi.connector.gradle.factory.SamplesTaskFactory
import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

/**
 * Plugin implementation for building connectors. The plugin "extends" the java-library plugin and adds the necessary 
 * tasks, configurations, etc for building connector archives. 
 */
@CompileStatic
class ConnectorPlugin implements Plugin<Project> {
    
    private static final Logger LOG = Logging.getLogger(ConnectorPlugin)

    @Override
    public void apply(Project project) {
        //plugins
        project.apply plugin: 'java-library'

        //repositories
        project.repositories.maven { MavenArtifactRepository repo ->
            repo.setName "boomisdk"
            repo.setUrl "https://boomisdk.s3.amazonaws.com/releases"
        }

        //extensions
        def connectorExtension = project.extensions.create("connector", ConnectorExtension, project)
        def openapiExtension = project.extensions.create("openAPIConnector", OpenAPIExtension, project)
        
        new CoreTaskFactory(project, connectorExtension, openapiExtension).registerFactory()
        new HelloWorldTaskFactory(project).registerFactory()
        new SamplesTaskFactory(project, connectorExtension).registerFactory()
    }
    
}
