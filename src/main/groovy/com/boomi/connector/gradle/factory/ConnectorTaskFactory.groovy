// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.gradle.factory

import com.boomi.connector.gradle.ConnectorArtifact
import com.boomi.connector.gradle.ConnectorExtension
import com.boomi.connector.gradle.task.ConnectorTaskAction
import groovy.transform.CompileStatic
import org.gradle.api.NamedDomainObjectProvider
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ExternalDependency
import org.gradle.api.artifacts.MutableVersionConstraint
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer

/**
 * Simple factory abstraction for registering related connector tasks 
 */
@CompileStatic
abstract class ConnectorTaskFactory {

    protected final Project _project
    protected final ConnectorExtension _connectorExtension
    private static final Logger LOG = Logging.getLogger(ConnectorTaskFactory)
    /**
     * Creates a new instance with the provided project and connector extension 
     */
    public ConnectorTaskFactory(Project project, ConnectorExtension ext) {
        _project = project
        _connectorExtension = ext
    }
    
    /**
     * Registers the implementation of factory instances and all of the tasks under it.
     * If an instance is registered as experimental, its tasks are registered as usual with a warning that the feature is still under development.
     * */
    void registerFactory() {
        addWarningIfExperimentalFeature()
        registerTasks()
    }
    
    /**
     * Convenience method to decide if the experimental warning message should be displayed or not.
     **/
    private void addWarningIfExperimentalFeature() {
        if (isExperimental()) {
            logExperimental(this.getClass().getSimpleName())
        }
    }
    
    private static void logExperimental(String className) {
        LOG.warn("The '${className}' is an experimental feature and is still under development.")
    }
    
    /**
     * Different Factory instances can choose to override this method and return true until its tasks are mature.
     * @return false
     */
    protected boolean isExperimental() {
        return false
    }
    
    /**
     * Register all of the tasks
     */
    protected abstract void registerTasks()
  
    /**
     * Creates a new generated source set with the provided name. Tasks generated for the source set will included in 
     * the boomi task group. This is not intended to create non-generaetd source sets.
     * 
     * @param name the name of the source set 
     * @return the newly created source set   
     */    
    protected SourceSet createSourceSet(String name) {
        String base = "$_project.buildDir/generated/src/$name"
        SourceSet sourceSet = getSourceSets().create(name)
        sourceSet.java.setSrcDirs(Collections.singletonList("$base/java"))
        sourceSet.resources.setSrcDirs(Collections.singletonList("$base/resources"))
        _project.tasks.findAll { Task task -> task.name =~ /.*(${name.uncapitalize()}|${name.capitalize()}).*/ }.each {
            it.group = ConnectorTaskAction.GROUP
        }
        sourceSet
    }

    /**
     * Adds a dependency on a connector artifact to the specified configuration. Currently only supports sdk
     * versioned artifacts.
     *
     * @param configurationName the name of the configuration where the dependency will be added  
     * @param artifact the artifact 
     * @param classifier the optional artifact classifier 
     */
    protected void addDependency(String configurationName, ConnectorArtifact artifact, String classifier = null) {
        _project.configurations[configurationName].withDependencies { deps ->
            def dep = artifact.dependency(_connectorExtension.sdkVersion.get())
            deps.add _project.dependencies.create(classifier ? "$dep:$classifier" : dep)
        }
    }
    
    /**
     * Adds a version preference for a connector artifact to keep the sdk version consistent. Currently only supports
     * sdk versioned artifacts. The preference will be applied to all configurations (current and yet to be added).
     *
     * @param artifact the artifact
     */
    protected void addVersionPreference(ConnectorArtifact artifact) {
        _project.configurations.all { Configuration conf ->
            conf.withDependencies { deps ->
                ExternalDependency dep = deps.withType(ExternalDependency).find { it.group == artifact.group && it.name == artifact.module }
                dep?.version { MutableVersionConstraint version -> version.prefer _connectorExtension.sdkVersion.get() }
            }
        }
    }

    /**
     * Registers a new connector lib configuration. The configuration will extended the specified parent configuration 
     * and will exclude connector artifacts that are not intended to be packaged in a connector. 
     * 
     * @param name the name of the configuration to register 
     * @param parentName the name of the parent configuration 
     * @return provider for the newly registered configuration 
     */
    protected NamedDomainObjectProvider<Configuration> registerConnectorLib(String name, String parentName) {
        _project.configurations.register(name, { Configuration conf ->
            conf.extendsFrom _project.configurations[parentName]
            ConnectorArtifact.exclusions().each { conf.exclude it }
        })
    }

    /**
     * Convenience method to look up a task provider and retrieve the task  
     * 
     * @param name the name of the task 
     * @return the task 
     */
    protected Task task(String name) {
        _project.tasks.named(name).get()
    }

    /**
     * Convenience method to get the main source set 
     * 
     * @return the source set 
     */
    protected SourceSet getMainSourceSet() {
        getSourceSets().getByName(SourceSet.MAIN_SOURCE_SET_NAME)
    }

    /**
     * Convenience method to access the source set container via project extensions 
     * 
     * @return the source set container 
     */
    private SourceSetContainer getSourceSets() {
        _project.extensions.findByType(SourceSetContainer)
    }
}
