// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.gradle.factory

import com.boomi.connector.gradle.ConnectorArtifact
import com.boomi.connector.gradle.ConnectorExtension
import com.boomi.connector.gradle.OpenAPIExtension
import com.boomi.connector.gradle.task.ConnectorArchiveAction
import com.boomi.connector.gradle.task.ConnectorConfigAction
import com.boomi.connector.gradle.task.ConnectorDescriptorAction
import com.boomi.connector.gradle.task.ConnectorJsonConfigAction
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.SourceSet

/**
 * Task factory responsible for registering the core plugin tasks that are used to build connector archives. 
 */
@CompileStatic
class CoreTaskFactory extends ConnectorTaskFactory {
    protected final OpenAPIExtension _openAPIExtension
    
    CoreTaskFactory(Project project, ConnectorExtension ext, OpenAPIExtension openAPIExt) {
        super(project, ext)
        this._openAPIExtension = openAPIExt
    }
    
    @Override
    public void registerTasks() {
        SourceSet sourceSet = getMainSourceSet()
        addDependencies()
        
        Task connectorArchive = new ConnectorArchiveAction(_project, sourceSet, "api").registerTask().get()
        connectorArchive.dependsOn new ConnectorConfigAction(_project, sourceSet, _connectorExtension, _openAPIExtension).registerTask()
        connectorArchive.dependsOn new ConnectorJsonConfigAction(_project, sourceSet, _openAPIExtension, _connectorExtension).registerTask()
        connectorArchive.dependsOn new ConnectorDescriptorAction(_project, sourceSet, _openAPIExtension, _connectorExtension).registerTask()
    
        _project.artifacts.add("archives", connectorArchive)
        //force consistent versions for sdk libs 
        ConnectorArtifact.sdkArtifacts().each {
            addVersionPreference(it)        
        }
    }
    
    /**
     * Add different Connector Artifacts relevant for the task with their appropriate configuration
     * */
    void addDependencies() {
        addDependency("implementation", ConnectorArtifact.SDK_API)
        addDependency("api", ConnectorArtifact.SDK_UTIL)
        addDependency("testImplementation", ConnectorArtifact.SDK_TEST_UTIL)
        _project.afterEvaluate {
            // If openAPIExtension is valid
            if (_openAPIExtension?.openApiSpecification || _openAPIExtension?.authType || _openAPIExtension?.operations) {
                _project.repositories.mavenCentral()
                addDependency("api", ConnectorArtifact.SDK_OPEN_API, null)
            }
        }
    }
}
 