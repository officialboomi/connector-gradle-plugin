// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.gradle.factory

import com.boomi.connector.gradle.ConnectorArtifact
import com.boomi.connector.gradle.ConnectorExtension
import com.boomi.connector.gradle.task.DependencyDownloadAction
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSet

/**
 * Task factory responsible for registering the connector samples tasks 
 */
@CompileStatic
class SamplesTaskFactory extends ConnectorTaskFactory {

    /**
     * Creates a new instance with the provided project and connector extension 
     */
    SamplesTaskFactory(Project project, ConnectorExtension ext) {
        super(project, ext)
    }
    
    @Override
    public void registerTasks() {
        SourceSet sourceSet = createSourceSet("connectorSamples")
        addDependency(sourceSet.implementationConfigurationName, ConnectorArtifact.SDK_SAMPLES, "sources")
        task(sourceSet.compileJavaTaskName).dependsOn new DependencyDownloadAction(_project, sourceSet, sourceSet.implementationConfigurationName).registerTask()
    }
}
