// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.gradle.factory

import com.boomi.connector.gradle.ConnectorArtifact
import com.boomi.connector.gradle.ConnectorExtension
import com.boomi.connector.gradle.task.ConnectorArchiveAction
import com.boomi.connector.gradle.task.ConnectorConfigAction
import com.boomi.connector.gradle.task.ConnectorJarAction
import com.boomi.connector.gradle.task.HelloWorldSourceAction
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.SourceSet

/**
 * Task factory responsible for registering the connector hello world tasks. 
 */
@CompileStatic
class HelloWorldTaskFactory extends ConnectorTaskFactory {    
   
    /**
     * Creates a new instance with the provided project 
     */
    HelloWorldTaskFactory(Project project) {
        super(project, new ConnectorExtension(project, HelloWorldSourceAction.CONNECTOR_CLASS_NAME))
    }
     
    @Override
    public void registerTasks() {
        SourceSet sourceSet = createSourceSet("connectorHelloWorld")      
        addDependency(sourceSet.implementationConfigurationName, ConnectorArtifact.SDK_API)
        addDependency(sourceSet.implementationConfigurationName, ConnectorArtifact.SDK_UTIL)

        def sourceTask = new HelloWorldSourceAction(_project, sourceSet).registerTask()
        task(sourceSet.compileJavaTaskName).dependsOn sourceTask
        task(sourceSet.processResourcesTaskName).dependsOn sourceTask
        
        Task jarTask = new ConnectorJarAction(_project, sourceSet).registerTask().get()
        jarTask.dependsOn new ConnectorConfigAction(_project, sourceSet, _connectorExtension).registerTask()
        
        new ConnectorArchiveAction(_project, sourceSet, sourceSet.implementationConfigurationName).registerTask()
    }
}
