/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle.domain.oauth2

import com.boomi.connector.gradle.validators.annotations.RequireValue
import com.boomi.connector.gradle.validators.annotations.ValidValues
import groovy.transform.CompileStatic

/**
 * Base container class representing all available OAUTH2 sub-fields.
 */
@CompileStatic
class OAuth2Field extends OAuth2ObjectContainerField {
    
    private static final Set<String> stringSubFields = ["accessTokenEndpoint","authorizationTokenEndpoint", "scope"] as Set<String>
    private static final Set<String> listSubFields = ["grantType"] as Set<String>
    private static final Set<String> keyValuePairFields = ["authorizationParameters", "accessTokenParameters"] as Set<String>

    
    def authorizationTokenEndpoint
    def accessTokenEndpoint
    def authorizationParameters
    def accessTokenParameters
    def scope
    @ValidValues(allowed = ["code", "client_credentials", "implicit", "jwt-bearer"])
    def grantType
    @RequireValue(onlyIf = {requiresJWTValidation()})
    def jwtParameters
    
    private void jwtParameters(Closure closure) {
        JWTParameters jwtParams = new JWTParameters()
        this.jwtParameters = getDelegatedClone(closure, jwtParams)
    }
    
   protected boolean requiresJWTValidation() {
        ((String) ((ListSubFieldConfig) grantType)?.defaultValue)?.equalsIgnoreCase('jwt-bearer') ||
                ((List) ((ListSubFieldConfig) this.grantType)?.includedValues)?.contains('jwt-bearer')
    }
    
    @Override
    protected Set<String> getStringSubFields() {
        return stringSubFields
    }
    
    @Override
    protected Set<String> getListSubFields() {
        return listSubFields
    }
    
    @Override
    protected Set<String> getKeyValuePairSubFields() {
        return keyValuePairFields
    }
    
    @Override
    protected Set<String> getBaseSubFields() {
        return Collections.EMPTY_SET
    }
}
