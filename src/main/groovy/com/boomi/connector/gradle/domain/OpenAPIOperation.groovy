/*
 * Copyright (c) 2021 Boomi, Inc.
*/

package com.boomi.connector.gradle.domain

import com.boomi.connector.gradle.validators.annotations.RequireValue
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic

/**
 * OpenAPIOperation provides a simple way to define the structure of each operations input
 **/
@CompileStatic
class OpenAPIOperation {

    def name
    @RequireValue
    Set<OpenAPIOperationObject> operationObjects = []

    OpenAPIOperation(def name) {
        this.name = name
    }
    
    
    @CompileDynamic
    def methodMissing(String schemaName, def args) {
        OpenAPIOperationObject object = new OpenAPIOperationObject(schemaName)
        List closureList = args as List
        def c = (closureList.get(0) as Closure).rehydrate(object, this, this)
        c.resolveStrategy = Closure.DELEGATE_ONLY
        c()
        validateOperationId(object.operationId, schemaName)
        operationObjects << object
        //check to make sure there are no duplicate schemas under an operation
        operationObjects = operationObjects.unique() as Set
    }
    
    def validateOperationId(def operationId, String schemaName) {
        // validation for a must have operationId
        if (operationId == null) {
            throw new IllegalArgumentException("operationId can't be empty for operation object : '${schemaName}' under '${name}'")
        }
        // validation to make sure both method and path values exist
        if (operationId instanceof OpenAPIOperationId) {
            if (!operationId.method  || !operationId.path ) {
                throw new IllegalArgumentException("Operation object: '${schemaName}' under '${name}' needs to have both method and path defined")
            }
        }
    }
}
