/*
 * Copyright (c) 2021 Boomi, Inc.
 */

package com.boomi.connector.gradle.domain

import com.boomi.connector.gradle.validators.annotations.RequireValue
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic

/**
 * The combination of method and path values will always be unique.
 * Can be used in the absence of an actual operationId. Implements {@link ObjectContainerField} for the method and path sub-field values to
 * be validated in accordance with the annotations applied.
 **/
@CompileStatic
class OpenAPIOperationId implements ObjectContainerField {
    
    @RequireValue
    def method
    @RequireValue
    def path
    
    @Override
    @CompileDynamic
    def methodMissing(String fieldName, def args) {
        this."$fieldName" = (args as List).get(0)
    }
}