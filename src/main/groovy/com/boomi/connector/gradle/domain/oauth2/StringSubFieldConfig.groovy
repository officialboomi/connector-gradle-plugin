/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle.domain.oauth2

import groovy.transform.CompileStatic

/**
 * Base class used to represent an OAUTH2 sub-field accepting free form inputs
 */
@CompileStatic
class StringSubFieldConfig extends BaseSubFieldConfig {
    
    def defaultValue
    
    StringSubFieldConfig(String subFieldName){
       this(subFieldName,null)
    }
    StringSubFieldConfig(String subFieldName, String defaultValue){
        super(subFieldName)
        this.defaultValue = defaultValue
    }
}
