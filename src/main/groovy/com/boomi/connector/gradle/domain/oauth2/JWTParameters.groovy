/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle.domain.oauth2

import com.boomi.connector.gradle.validators.annotations.ValidValues
import groovy.transform.CompileStatic

/**
 * Base container class representing all available JWTParameter sub-fields under OAUTH2 config.
 */
@CompileStatic
class JWTParameters extends OAuth2ObjectContainerField {
    
    private static final Set<String> stringSubFields = ["issuer", "subject", "audience", "expiration"] as Set<String>
    private static final Set<String> listSubFields = ["signatureAlgorithms"] as Set<String>
    private static final Set<String> keyValuePairFields = ["extendedClaims"] as Set<String>
    private static final Set<String> baseFieldSubFields = ["signatureKey"] as Set<String>
    
    @ValidValues(allowed = ["NONE", "SHA256withRSA"])
    def signatureAlgorithms
    def issuer
    def subject
    def audience
    def expiration
    def signatureKey
    def extendedClaims
    
    @Override
    protected Set<String> getStringSubFields() {
        return stringSubFields
    }
    
    @Override
    protected Set<String> getListSubFields() {
        return listSubFields
    }
    
    @Override
    protected Set<String> getKeyValuePairSubFields() {
        return keyValuePairFields
    }
    
    @Override
    protected Set<String> getBaseSubFields() {
        return baseFieldSubFields
    }
}
