/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle.domain.oauth2

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic

/**
 * Base class used to represent common elements of OAUTH2 sub-fields
 */
@CompileStatic
class BaseSubFieldConfig {
    
    def helpText
    def access = "enabled"
    def subFieldName
    
    BaseSubFieldConfig(String subFieldName) {
        this.subFieldName = subFieldName
    }
    
    @CompileDynamic
    def methodMissing(String fieldName, def args) {
        if (fieldName == "includedValues") {
            this."$fieldName" = (args as List).unique(false)
        } else {
            this."$fieldName" = (args as List).get(0)
        }
    }
}