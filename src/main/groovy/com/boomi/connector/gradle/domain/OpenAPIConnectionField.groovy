/*
 * Copyright (c) 2021 Boomi, Inc.
 */

package com.boomi.connector.gradle.domain

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic

@CompileStatic
class OpenAPIConnectionField {
  
  def label
  def helpText
  def defaultValue
  def includedValues
  
  OpenAPIConnectionField(def defaultValue) {
    this.defaultValue = defaultValue
  }
  
  @CompileDynamic
  def methodMissing(String fieldName, def args) {
    if (fieldName == "includedValues") {
      this.includedValues = (args as List).unique(false)
    } else {
      this."$fieldName" = (args as List).get(0)
    }
  }
}

