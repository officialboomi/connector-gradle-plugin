/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle.domain

/**
 * Abstraction to signify a FieldConfig has sub-fields within it which needs to be validated for its input values.
 * The general expectation is to use this interface and check for sub fields instead of checking individual class instance types.
 * */
interface ObjectContainerField {
    
    def methodMissing(String fieldName, def args)
}