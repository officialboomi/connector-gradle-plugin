// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.gradle.domain.oauth2

import com.boomi.connector.gradle.domain.ObjectContainerField
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic

/**
 * Base OAuth2 class that represents all OAuth2 sub fields including {@link JWTParameters} and its subsequent sub fields. Also instantiates the
 * individual fields based on its pre-determined type i.e if its String, List, KeyValuePair etc. Implementing classes can implement their own
 * getters returning a list of fields under the parent class , classified by their type.*/
@CompileStatic
abstract class OAuth2ObjectContainerField implements ObjectContainerField {
    
    @Override
    @CompileDynamic
    def methodMissing(String fieldName, def args) {
        def value = (args as List).get(0)
        if (value instanceof Closure) {
            BaseSubFieldConfig subFieldConfig = null
            if (getStringSubFields()?.contains(fieldName)) {
                subFieldConfig = new StringSubFieldConfig(fieldName)
            } else if (getListSubFields()?.contains(fieldName)) {
                subFieldConfig = new ListSubFieldConfig(fieldName)
            } else if (getKeyValuePairSubFields()?.contains(fieldName)) {
                subFieldConfig = new KeyValuePairSubFieldConfig(fieldName)
            } else if (getBaseSubFields()?.contains(fieldName)) {
                subFieldConfig = new BaseSubFieldConfig(fieldName)
            }
            this."$fieldName" = getDelegatedClone(value, subFieldConfig)
        } else if (value instanceof String) {
            StringSubFieldConfig subFieldConfig = new StringSubFieldConfig(fieldName, value)
            this."$fieldName" = subFieldConfig
        }
    }
    
    protected getDelegatedClone(Closure closure, Object delegate) {
        def clone = closure.rehydrate(delegate, this, this)
        clone.resolveStrategy = Closure.DELEGATE_ONLY
        clone()
        delegate
    }
    
    /**
     * Retrieves the list of sub-field names that are of type {@link StringSubFieldConfig}
     * @return a Set of String sub-field names
     */
    protected abstract Set<String> getStringSubFields();
    
    /**
     * Retrieves the list of sub-field names that are of type {@link ListSubFieldConfig}
     * @return a Set of String sub-field names
     */
    protected abstract Set<String> getListSubFields();
    
    /**
     * Retrieves the list of sub-field names that are of type {@link KeyValuePairSubFieldConfig}
     * @return a Set of String sub-field names
     */
    protected abstract Set<String> getKeyValuePairSubFields();
    
    /**
     * Retrieves the list of sub-field names that are of type {@link BaseSubFieldConfig}
     * @return a Set of String sub-field names
     */
    protected abstract Set<String> getBaseSubFields();
}