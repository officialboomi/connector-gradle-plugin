/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle.domain.oauth2

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic

/**
 * Base class used to represent a dropdown OAUTH2 sub-field
 */
@CompileStatic
class ListSubFieldConfig extends StringSubFieldConfig {
    
    def includedValues
    
    ListSubFieldConfig(String subFieldName) {
        this(subFieldName,null)
    }
    ListSubFieldConfig(String subFieldName, String defaultValue) {
        super(subFieldName, defaultValue)
    }
}
