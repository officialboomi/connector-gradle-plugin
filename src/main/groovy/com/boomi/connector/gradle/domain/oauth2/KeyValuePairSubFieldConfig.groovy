/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle.domain.oauth2

import groovy.transform.CompileStatic

/**
 * Base class used to represent a key-value pair OAUTH2 sub-field
 */
@CompileStatic
class KeyValuePairSubFieldConfig extends BaseSubFieldConfig {
   
    def keyValuePairs
    
    KeyValuePairSubFieldConfig(String subFieldName){
        super(subFieldName)
    }
    
    def methodMissing( String fieldName, def args ) {
        if(fieldName == "keyValuePairs"){
            if((args as List ).get(0) instanceof Map ) {
                this.keyValuePairs = (args as List).get(0)
            } else {
                throw new IllegalArgumentException(
                        "Please check the value defined for 'keyValuePairs' sub-field under $subFieldName. Proper value e.g : " +
                        "key1:value1, key2:value2" )
            }
        }
    }
}
