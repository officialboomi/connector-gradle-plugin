/*
 * Copyright (c) 2021 Boomi, Inc.
 */

package com.boomi.connector.gradle.domain

import com.boomi.connector.gradle.validators.annotations.RequireValue
import groovy.transform.CompileStatic

/**
 * Define the structure of each operation. OperationId can either be defined as
 * a string or as a combination of 'path' and 'method' values
 **/
@CompileStatic
class OpenAPIOperationObject implements Comparable<OpenAPIOperationObject> {
    
    String schemaName
    @RequireValue
    def operationId
    
    OpenAPIOperationObject(String schemaName) {
        this.schemaName = schemaName
    }

    private void operationId(String operationId) {
        this.operationId = operationId
    }
    
    private void operationId(@DelegatesTo(OpenAPIOperationId) Closure c) {
        OpenAPIOperationId opId = new OpenAPIOperationId()
        def clone = c.rehydrate(opId, this, this)
        clone.resolveStrategy = Closure.DELEGATE_ONLY
        clone()
        this.operationId = opId
    }
    
    @Override
    int compareTo(OpenAPIOperationObject o) {
        int returnCode = schemaName <=> o.schemaName
        if (returnCode == 0) {
            throw new Exception(
                    "You already have '${schemaName} ' mentioned under the parent action. Please verify there are no "
                            + "duplicates.")
        }
        returnCode
    }
}
