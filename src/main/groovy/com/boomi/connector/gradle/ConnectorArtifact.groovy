// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.gradle

import groovy.transform.CompileStatic

/**
 * Enumeration of connector development artifacts. "Excluded" artifacts are those that should not be packaged in a 
 * connector archive.    
 */
@CompileStatic
enum ConnectorArtifact {   
    
    // boomi artifacts 
    BOOMI_UTIL(Group.UTIL, "boomi-util", false),
    BOOMI_JSON_UTIL(Group.UTIL, "boomi-json-util", false),
    COMMON_LITE(Group.UTIL, "common-lite", false),
    COMMON_SDK("com.boomi.commonsdk", "common-sdk", false),
    SDK_API(Group.SDK, "connector-sdk-api"),
    SDK_MODEL(Group.SDK, "connector-sdk-model", false),
    SDK_OPEN_API(Group.SDK, "connector-sdk-openapi", false),
    SDK_REST(Group.SDK, "connector-sdk-rest", false),
    SDK_SAMPLES(Group.SDK, "connector-sdk-samples"),
    SDK_TEST_UTIL(Group.SDK, "connector-sdk-test-util", false),
    SDK_UTIL(Group.SDK, "connector-sdk-util", false), 
    SDK_WS(Group.SDK, "connector-sdk-ws", false)
    
    String group
    String module
    boolean excluded
    
    /**
     * Creates a new instance of an excluded artifact 
     */
    ConnectorArtifact(def group, def module) {
        this(group, module, true)
    }
    
    /**
     * Creates a new instance of an artifact 
     */
    ConnectorArtifact(def group, def module, boolean excluded) {
        this.group = group;
        this.module = module;
        this.excluded = excluded;
    }
    
    /**
     * Returns the dependency notation for this artifact in the form group:module:version. The version is optional and 
     * will excluded if no value is provided.  
     * 
     * @param version the optional version 
     * @return the dependency notation 
     */
    String dependency(def version = null) {
        def dependency = "$group:$module"
        version ? "$dependency:$version" : dependency  
    }    
    
    /**
     * Returns an iterable containing the exclusion notation for excluded artifacts. Note exclusions use "module" for 
     * the artifact unlike dependencies which use "name".  
     */
    static Iterable<Map<String, String>> exclusions() {
        values().findAll { it.excluded }.collect { [group: it.group, module: it.module] as Map }
    }
    
    /**
     * Returns an iterable containing all of the artifacts in the sdk group. 
     */
    static Iterable<ConnectorArtifact> sdkArtifacts() {
        values().findAll { it.group == Group.SDK }
    }
    
    /**
     * Common group definitions 
     */
    private static class Group {
        
        public static String UTIL = "com.boomi.util"
        public static String SDK = "com.boomi.connsdk"

    }
}
