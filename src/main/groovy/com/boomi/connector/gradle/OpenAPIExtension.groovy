/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle

import com.boomi.connector.gradle.domain.oauth2.OAuth2Field
import com.boomi.connector.gradle.domain.OpenAPIConnectionField
import com.boomi.connector.gradle.domain.OpenAPIOperation
import com.boomi.connector.gradle.validators.annotations.RequireValue
import com.boomi.connector.gradle.validators.annotations.ValidValues
import groovy.transform.CompileStatic
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project

/**
 * Simple extension for configuring the OpenAPI connector's connection and operation inputs
 **/
@CompileStatic
class OpenAPIExtension {
  
  @RequireValue
  def openApiSpecification
  def server
  @RequireValue
  @ValidValues(allowed = ["NONE", "BASIC", "CUSTOM", "DIGEST", "OAUTH2"])
  def authType
  @RequireValue
  NamedDomainObjectContainer<OpenAPIOperation> operations
  @ValidValues(allowed =  ["GLOBAL", "CONNECTOR_SHAPE", "IGNORED"])
  def cookieScope
  def includePrivateCert
  def includePublicCert
  def oAuth2Config

    OpenAPIExtension(Project project) {
      this.operations = project.container(OpenAPIOperation)
    }

    def operations(Closure c) {
      operations.configure c
    }

    def authType(String type) {
      this.authType = type?.toUpperCase()
    }

    def authType(Closure c) {
      this.authType = getConnectionFieldFromClosure(c)
    }

    def cookieScope(String cookie) {
      this.cookieScope = cookie
    }

    def cookieScope(Closure c) {
      this.cookieScope = getConnectionFieldFromClosure(c)
    }

    def openApiSpecification(String spec) {
      this.openApiSpecification = spec
    }

    def server(String server) {
      this.server = server
    }

    def includePrivateCert(Closure c) {
      this.includePrivateCert = getConnectionFieldFromClosure(c)
    }

    def includePrivateCert(Boolean include) {
      this.includePrivateCert = include
    }

    def includePublicCert(Closure c) {
      this.includePublicCert = getConnectionFieldFromClosure(c)
    }

    def includePublicCert(Boolean include) {
      this.includePublicCert = include
    }
  
    def OAuth2Config(Closure closure){
      OAuth2Field config = new OAuth2Field()
      def clone = closure.rehydrate(config, this, this)
      clone.resolveStrategy = Closure.DELEGATE_ONLY
      clone()
      this.oAuth2Config = config
    }
    
    static def getConnectionFieldFromClosure(Closure closure) {
      OpenAPIConnectionField connectionField = new OpenAPIConnectionField("")
      def clone = closure.rehydrate(connectionField, this, this)
      clone.resolveStrategy = Closure.DELEGATE_ONLY
      clone()
      connectionField
    }
}
