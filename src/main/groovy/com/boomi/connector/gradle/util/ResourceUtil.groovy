package com.boomi.connector.gradle.util

import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

import groovy.transform.CompileStatic

/**
 * Utility methods for working with resources
 */
@CompileStatic
final class ResourceUtil {

    private static final Logger LOG = Logging.getLogger(ResourceUtil)

    private ResourceUtil() { }


    /**
     * Convenience method to get a resource stream
     * 
     * @param name the name of the resource 
     * @return stream containing the resource contents  
     */
    public static InputStream getResource(String name) {
        ResourceUtil.classLoader.getResourceAsStream(name)
    }

    /**
     * Loads a properties instance from the specified resource. If the properties cannot be loaded, a warning is logged
     * and an empty instance is returned. 
     * 
     * @param name the name of the resource 
     * @return the loaded properties 
     */
    public static Properties loadProperties(String name) {
        Properties props = new Properties()
        try {
            getResource(name)?.withCloseable {
                props.load(it)
            }
        } catch (IOException e) {
            LOG.warn("unable to load $name", e)
        }
        props
    }

}
