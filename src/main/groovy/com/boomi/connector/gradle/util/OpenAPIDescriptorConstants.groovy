package com.boomi.connector.gradle.util

public final class OpenAPIDescriptorConstants {
    
    public static final String AUTHTYPE_BASIC = "BASIC"
    public static final String AUTHTYPE_CUSTOM = "CUSTOM"
    public static final String AUTHTYPE_DIGEST = "DIGEST"
    public static final String AUTHTYPE_OAUTH2 = "OAUTH2"

    public static final String FIELD_ELEMENT = "field"
    public static final String OAUTH_CONFIG_ELEMENT = "oauth2FieldConfig"
    public static final String SCOPE_ELEMENT = "scope"

    public static final String FIELDID_URL = "url"
    public static final String FIELDID_AUTH_TYPE = "auth"
    public static final String FIELDID_COOKIE_SCOPE = "cookieScope"
    public static final String FIELDID_PRIVATECERTIFICATE = "privateCertificate"
    public static final String FIELDID_PUBLICCERTIFICATE = "publicCertificate"
    public static final String FIELDID_USERNAME = "username"
    public static final String FIELDID_PASSWORD = "password"
    public static final String FIELDID_PREEMPTIVE = "preemptive"
    public static final String FIELDID_CUSTOM_AUTH = "customAuthCredentials"
    public static final String FIELDID_OAUTH2 = "oauthContext"
    
    public static final String TYPE_STRING = "string"
    public static final String TYPE_PASSWORD = "password"
    public static final String TYPE_BOOLEAN = "boolean"
    public static final String TYPE_OAUTH = "oauth"
    public static final String TYPE_PRIVATECERTIFICATE = "privatecertificate"
    public static final String TYPE_PUBLICCERTIFICATE = "publiccertificate"
    
    public static final String LABEL_AUTH_TYPE = "Authentication Type"
    public static final String LABEL_COOKIE_SCOPE = "Cookie Scope"
    public static final String LABEL_PRIVATECERTIFICATE = "Client SSL Certificate"
    public static final String LABEL_PUBLICCERTIFICATE = "Trust SSL Server Certificate"
    public static final String LABEL_USERNAME = "Username"
    public static final String LABEL_PASSWORD = "Password"
    public static final String LABEL_SERVER = "Server"
    public static final String LABEL_PREEMPTIVE_AUTH = "Preemptive authentication"
    public static final String LABEL_CUSTOM_AUTH = "Custom Authentication Credentials"
    public static final String LABEL_OAUTH2 = "OAuth 2.0"
    
    public static final String DEFAULT_VALUE = "defaultValue"
    public static final String HELP_TEXT = "helpText"
    public static final String URL_ENDPOINT = "url"
    public static final String ALLOWED_VALUE = "allowedValue"
    public static final String VALUE = "value"
    public static final String PARAMETER = "parameter"
    public static final String ACCESS = "access"
    public static final String NAME = "name"
}
