/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle.validators.annotations

import static java.lang.annotation.ElementType.FIELD
import static java.lang.annotation.RetentionPolicy.RUNTIME

import java.lang.annotation.Retention
import java.lang.annotation.Target

/**
 * Defines a new annotation and provides the ability to specify if a field is required or not. Also provides the ability to make the value of such
 * field conditional with the onlyIf attribute. Can be used alone without specifying 'onlyIf' attribute as well.
 * <pre>
 *     @RequireValue
 *     def field1
 *     @RequireValue (onlyIf = { field1 })  // Essentianlly means that field2 is required onlyIf field1 is not null
 *     def field2
 * </pre>
 */
@Retention(RUNTIME)
@Target(FIELD)
@interface RequireValue {
    
    Class onlyIf() default { true }
    
}
