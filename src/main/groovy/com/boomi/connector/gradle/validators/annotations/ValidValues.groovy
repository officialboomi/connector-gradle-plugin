/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle.validators.annotations

import java.lang.annotation.Retention
import static java.lang.annotation.ElementType.FIELD
import static java.lang.annotation.RetentionPolicy.RUNTIME
import java.lang.annotation.Target

/**
 * Defines a new field level annotation and provides the ability to define allowed values for a particular field with the 'allowed' attribute.
 * If a field using this annotation has a value which is not mentioned in the 'allowed' attributes list, that will result in a failed validation
 * and an Exception will be thrown.
 * <pre>
 * @ValidValues (allowed = {"value1,value2"}) // Essentially limits legal values for field1 to be 'value1' & 'value2'
 *  def field1
 * </pre>
 */
@Retention(RUNTIME)
@Target(FIELD)
@interface ValidValues {
    
    String[] allowed() default []
}