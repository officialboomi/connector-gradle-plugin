/*
 * Copyright (c) 2021 Boomi, Inc.
 */
package com.boomi.connector.gradle.validators

import com.boomi.connector.gradle.domain.ObjectContainerField
import com.boomi.connector.gradle.validators.annotations.RequireValue
import com.boomi.connector.gradle.validators.annotations.ValidValues

/**
 * Simple validator class enforcing configuration inputs provided under 'openAPIConnector' extension, against rules specific to each connection or
 * operation field. Throws an  {@link IllegalArgumentException} in case of rule violation.*/
class OpenAPIGroovyInputValidator {
    
    private static final String OPENAPIEXTENSION_CLASSNAME = "com.boomi.connector.gradle.OpenAPIExtension"
    
    static void validate(def obj) {
        
        def c = getActualClassWithFields(obj)
        c?.declaredFields?.each {
            if (!it.isSynthetic()) {
                def fieldValue = obj."$it.name"
                if (fieldValue) {
                    ValidValues requireValues = it.getAnnotation(ValidValues)
                    if (isParentContainerField(fieldValue)) {
                        //e.g OAuth2Field & JWTParameters & OperationId parent nodes
                        validate(fieldValue)
                    } else if (isOperationField(fieldValue)) {
                        //operations
                        fieldValue.each {validate(it)}
                    } else {
                        // individual string or closure fields
                        if (containsImproperFieldValue(requireValues, fieldValue)) {
                            throw new IllegalArgumentException("$it.name value must be one of ${requireValues?.allowed()}")
                        }
                        if (containsImproperAccessFieldValue(fieldValue)) {
                            throw new IllegalArgumentException("$it.name's 'access' value must be one of 'hidden, readOnly, enabled'")
                        }
                    }
                } else if (it.getAnnotation(RequireValue)?.onlyIf()?.newInstance(obj, obj)?.call()) {
                    throw new IllegalArgumentException("$it.name is required")
                }
            }
        }
    }
    
    /**
     * This selection of class is mainly done to support recursion
     * and to extract OpenAPIExtension class from the decorated extension object
     * @param obj
     **/
    private static Class<?> getActualClassWithFields(obj) {
        Class c
        if (((obj.metaClass).theClass).name.contains(OPENAPIEXTENSION_CLASSNAME)) {
            c = Class.forName(OPENAPIEXTENSION_CLASSNAME)
        } else {
            c = obj.class
        }
        c
    }
    // Helper methods //
    private static boolean containsImproperFieldValue(ValidValues validValues, fieldValue) {
        if (fieldValue instanceof String) {
            return validValues && !validValues.allowed().contains(fieldValue)
        }
        return containsImproperDefaultOrIncludedFieldValue(fieldValue, validValues)
    }
    
    private static boolean containsImproperDefaultOrIncludedFieldValue(fieldValue, ValidValues validValues) {
        if (validValues && !hasDefaultValues(fieldValue) && !hasIncludedValues(fieldValue)) {
            return true
        }
        return validValues && ((hasDefaultValues(fieldValue) && !validValues.allowed().contains(fieldValue.defaultValue)) ||
                (hasIncludedValues(fieldValue) && !validValues.allowed().asType(List).containsAll(fieldValue?.includedValues)))
    }
    
    private static boolean containsImproperAccessFieldValue(fieldValue) {
        if (hasAccessFieldValue(fieldValue)) {
            return (fieldValue?.access && !["hidden", "readOnly", "enabled"].contains(fieldValue.access))
        }
        false
    }
    
    private static boolean hasDefaultOrIncludedValues(fieldValue) {
        hasDefaultValues(fieldValue) || hasIncludedValues(fieldValue)
    }
    
    private static boolean hasDefaultValues(fieldValue) {
        fieldValue.hasProperty('defaultValue') && fieldValue.defaultValue?.length() > 0 // mainly to cover "" value
    }
    
    private static boolean hasIncludedValues(fieldValue) {
        fieldValue.hasProperty('includedValues') && containsAtLeastOneIncludedValuesEntry(fieldValue?.includedValues)
    }
    
    private static boolean hasAccessFieldValue(fieldValue) {
        fieldValue.hasProperty('access')
    }
    
    private static boolean isOperationField(fieldValue) {
        fieldValue instanceof Collection
    }
    
    private static boolean isParentContainerField(fieldValue) {
        fieldValue instanceof ObjectContainerField
    }
    
    private static boolean containsAtLeastOneIncludedValuesEntry(def includedValues) {
        includedValues?.any {item -> (item?.length() > 0)
        }
    }
}
