// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.gradle

import com.boomi.connector.gradle.util.TestProps
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import java.util.zip.ZipFile

/**
 * Base container class with common resources and methods used in different functional specifications
 */
class BaseFSpec extends Specification {
    
    protected static final def PROJECT = "connector"
    protected static final def VERSION = 1.0
    protected static final def ARTIFACT = "$PROJECT-$VERSION"
    protected static final def DEFAULT_SDK_VERSION = TestProps.expectedSdkVersion()
    
    @Rule
    TemporaryFolder projectDir
    File buildDir
    File distributions
    File generatedSrc
    File buildFile
    File settingsFile
    
    def setup() {
        buildDir = "${projectDir.root}/build" as File
        distributions = "$buildDir/distributions" as File
        generatedSrc = "$buildDir/generated/src" as File
        settingsFile = projectDir.newFile("settings.gradle") << "rootProject.name = '$PROJECT'" 
        
        newBuildFile() << """
            connector {
                className "theClass"
            }
        """
    }
    
    /**
     * executes the connectorArchive task and returns a map of files (not directories) in the connector archive
     *
     * the values in the map are closures/functions that can be called to obtain the contents of the file.*/
    def connectorArchive = {
        build("connectorArchive")
        
        def car
        distributions.eachFile {
            assert !car
            assert it.name == "$ARTIFACT-car.zip"
            car = it
        }
        
        zipEntries(car)
    }
    
    def zipEntries = {
        def zip = new ZipFile(it)
        def text = {entry ->
            zip.getInputStream(entry).withCloseable {
                return it.text
            }
        }
        zip.entries().findAll {!it.isDirectory()}.collectEntries {[(it.name): text.curry(it)]}
    }
    
    def getSourceJar = {ConnectorArtifact artifact, def version = DEFAULT_SDK_VERSION ->
        def group = artifact.group.replaceAll(/\./, /\//)
        def module = artifact.module
        def url = "https://boomisdk.s3.amazonaws.com/releases/$group/$module/$version/${module}-${version}-sources.jar"
        def jar = projectDir.newFile()
        jar.withOutputStream {out ->
            new URL(url).openStream().withCloseable {data -> out.write(data.bytes)
            }
        }
        zipEntries(jar)
    }
    
    def generatedConfig = {sdkVersion = DEFAULT_SDK_VERSION, className ->
        XmlUtil.serialize(new StreamingMarkupBuilder().bind {
            GenericConnector(strictClassLoading: true, sdkApiVersion: sdkVersion) {
                connectorClassName(className)
            }
        })
    }
    
    def build = {args -> createRunner(args).build()
    }
    
    def buildAndFail = {args -> createRunner(args).buildAndFail()
    }
    
    def createRunner = {args -> GradleRunner.create().withPluginClasspath().withProjectDir(projectDir.root).withArguments(args)
    }
    
    def newBuildFile = {
        if (buildFile) {
            buildFile.delete()
        }
        
        buildFile = projectDir.newFile("build.gradle") << """
            plugins {
                id 'com.boomi.connector'
            }
            version = "${VERSION}"
        """
    }
    
    def text = {file -> (file as File).text
    }
    
    def resource = {name ->
        BaseFSpec.classLoader.getResourceAsStream(name)?.withCloseable {
            return it.text
        }
    }
    
    def countMatches = {String str, String findStr ->
        def lastIndex = 0, count = 0
        while (lastIndex != -1) {
            lastIndex = str.indexOf(findStr, lastIndex)
            if (lastIndex != -1) {
                count++
                lastIndex += findStr.length()
            }
        }
        count
    }
    
    def connectorExtension = {sdkVersion, className ->
        return """
            connector {
                sdkVersion "$sdkVersion"
                className "$className"
            }
        """
    }
    
    def sdkUtilDependency = {version, configuration ->
        return """connector {
                sdkVersion "$version"
            }
            dependencies {
                ${configuration} "com.boomi.connsdk:connector-sdk-util"
            }
        """
    }
}
