// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.gradle

import groovy.io.FileType
import org.gradle.testkit.runner.TaskOutcome

/**
 * Functional specification for the connector plugin 
 *
 * TODO extract base class, create separate functional spec for each task factory
 * */
class ConnectorPluginFSpec extends BaseFSpec {


    private static final String SAMPLE_PACKAGE = "com.boomi.connector.sample"
    private static final String SAMPLE_CLASS_DIR = SAMPLE_PACKAGE.replaceAll(/\./, /\//)
    private static final String HELLO_WORLD_CLASS_NAME = "HelloWorldConnector"
    private static final String HELLO_WORLD_CONNECTOR_CLASS = "${SAMPLE_PACKAGE}.${HELLO_WORLD_CLASS_NAME}"
    private static final String HELLO_WORLD_DESCRIPTOR = "HelloWorld-descriptor.xml"
    private static final def SDK_ARTIFACTS = ConnectorArtifact.values().findAll {it.group == "com.boomi.connsdk"}

    def "assemble task depends on connectorArchive task"() {
        when:
        def carTask = build("assemble").task(":connectorArchive")

        then:
        carTask?.outcome == TaskOutcome.SUCCESS
    }

    def "connectorArchive packages correct artifacts"() {
        when:
        def carFile = connectorArchive()
        def artifact = ConnectorArtifact.SDK_UTIL

        then:
        carFile.size() == 5
        carFile["lib/${ARTIFACT}.jar"]
        carFile["lib/${artifact.module}-${DEFAULT_SDK_VERSION}.jar"]
        // generated config
        carFile["META-INF/connector-config.xml"]
        carFile["META-INF/MANIFEST.MF"]
    }

    def "connectorArchive packages connectorLib dependencies"() {
        buildFile << """
            repositories {
                mavenCentral()
            }
            dependencies {
                connectorLib "junit:junit:4.12"
            }
        """

        when:
        def carFile = connectorArchive()

        then:
        carFile["lib/junit-4.12.jar"]
        carFile["lib/hamcrest-core-1.3.jar"]
    }

    def "connectorArchive packages api dependencies"() {
        buildFile << """
            repositories {
                mavenCentral()
            }
            dependencies {
                api "junit:junit:4.12"
            }
        """

        when:
        def carFile = connectorArchive()

        then:
        carFile["lib/junit-4.12.jar"]
        carFile["lib/hamcrest-core-1.3.jar"]
    }

    def "connectorArchive does not package implementation dependencies"() {
        buildFile << """
            repositories {
                mavenCentral()
            }
            dependencies {
                implementation "junit:junit:4.12"
            }
            connectorConfig.enabled = false
        """

        when:
        def carFile = connectorArchive()
        def artifact = ConnectorArtifact.SDK_UTIL

        then:
        carFile.size() == 4
        carFile["lib/${ARTIFACT}.jar"]
        carFile["lib/${artifact.module}-${DEFAULT_SDK_VERSION}.jar"]
        carFile["META-INF/MANIFEST.MF"]
    }

    def "connectorArchive packages sdk dependencies"() {
        def artifact = ConnectorArtifact.SDK_UTIL

        buildFile << """
            dependencies {
                connectorLib "${artifact.dependency()}"
            }
        """

        when:
        def carFile = connectorArchive()
        
        then:
        carFile["lib/${artifact.module}-${DEFAULT_SDK_VERSION}.jar"]
        !carFile.find { it.key =~ /lib\/(connector-sdk-api|common-sdk).*/ }
    }

    def "connectorArchive packages META-INF resources and overwrites generated connector config"() {
        def project = new FileTreeBuilder(projectDir.root)

        project.src {
            main {
                resources {
                    "META-INF" {
                        "packaged"("yes")
                        "connector-descriptor.xml"("<GenericConnectorDescriptor/>")
                        "connector-config.xml"("<GenericConnector/>")
                    }
                    "not-packaged"("no")
                    "connector-config.xml"("<NotGenericConnector/>")
                }
            }
        }
        
        def sdkVersion = DEFAULT_SDK_VERSION
        def className = "the.class.name"
        buildFile << connectorExtension(sdkVersion, className)

        when:
        def carFile = connectorArchive()

        then:
        carFile["META-INF/packaged"]?.call() == "yes"
        carFile["META-INF/connector-descriptor.xml"]?.call() == "<GenericConnectorDescriptor/>"
        carFile["META-INF/connector-config.xml"]?.call() == "<GenericConnector/>"
        !carFile["not-packaged"]
    }
    
    def "connectorArchive does not overwrite connector-config.xml resource when generation is disabled"() {
        def project = new FileTreeBuilder(projectDir.root)

        project.src {
            main {
                resources {
                    "META-INF" {
                        "connector-descriptor.xml"("<GenericConnectorDescriptor/>")
                        "connector-config.xml"("<NotGeneratedGenericConnector/>")
                    }
                }
            }
        }

        buildFile << """
            connectorConfig.enabled = false
        """

        when:
        def carFile = connectorArchive()

        then:
        carFile["META-INF/connector-config.xml"]?.call() == "<NotGeneratedGenericConnector/>"
        carFile["META-INF/connector-descriptor.xml"]?.call() == "<GenericConnectorDescriptor/>"
    }
    
    def "connector sdk version can be overridden"() {
        def version = "2.5.2"

        buildFile << connectorExtension(version, null)

        when:
        def result = build("dependencies")
        
        then:
        result.output.contains ConnectorArtifact.SDK_API.dependency(version)
    }

    def "connectorArchive does not package excluded connector artifacts"() {
        def artifacts = ConnectorArtifact.values()

        buildFile << """
                repositories {
                    mavenCentral()
                }
                   dependencies {
                """
        artifacts.each {
            buildFile << "connectorLib \"${it.dependency("latest.release")}\", { transitive = false }\n"
        }
        buildFile << "}"
        
        when:
        def carFile = connectorArchive()
        
        then:
        artifacts.each {artifact ->
            def included = carFile.find {it.key ==~ /lib\/${artifact.module}-.*.jar/} as boolean
            assert included != artifact.excluded, "verifying $carFile"
        }
    }
    
    def "connectorDescriptor task is skipped for plain connector"() {
        def task = ":connectorDescriptor"

        when:
        def result = build(task).task(task)

        then:
        result?.outcome == TaskOutcome.SKIPPED
    }

    def "connectorJsonConfig task is skipped for plain connector"() {
        def task = ":connectorJsonConfig"

        when:
        def result = build(task).task(task)

        then:
        result?.outcome == TaskOutcome.SKIPPED
    }

    def "connectorConfig requires connector class name"() {
        def task = ":connectorConfig"
        newBuildFile()

        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorHelloWorldSource generates classes and desriptor"() {
        def base = "$generatedSrc/connectorHelloWorld"
        def sources = "$base/java/$SAMPLE_CLASS_DIR"
        def resources = "$base/resources"
        
        when:
        build("connectorHelloWorldSource")
        
        then:
        text("$sources/HelloWorldConnector.java") == resource("HelloWorldConnector.java")
        text("$sources/HelloWorldOperation.java") == resource("HelloWorldOperation.java")
        text("$resources/META-INF/connector-descriptor.xml") == resource(HELLO_WORLD_DESCRIPTOR)
    }
    
    def "connectorHelloWorld is up to date when generated source exists"() {
        def task = ":connectorHelloWorldSource"
        
        when:
        build(task)
        def result = build(task).task(task)
        
        then:
        result.outcome == TaskOutcome.UP_TO_DATE
    }
    
    def "connectorHelloWorld is not up to date when generated source is modified"() {
        def task = ":connectorHelloWorldSource"
        
        when:
        build(task)
        build("clean")
        def result = build(task).task(task)
        
        then:
        result.outcome == TaskOutcome.SUCCESS
    }
    
    def "connectorHelloWorldJar packages compiled source"() {
        when:
        buildFile << """
            repositories {
                mavenCentral()
            }
        """
        build("connectorHelloWorldJar")
        def jar = zipEntries("$buildDir/libs/connectorHelloWorld-${VERSION}.jar")
        
        then:
        jar["$SAMPLE_CLASS_DIR/${HELLO_WORLD_CLASS_NAME}.class"]
        jar["$SAMPLE_CLASS_DIR/HelloWorldOperation.class"]
        jar["META-INF/MANIFEST.MF"]
        jar["META-INF/connector-descriptor.xml"].call() == resource(HELLO_WORLD_DESCRIPTOR)
        jar["META-INF/connector-config.xml"].call() == generatedConfig(HELLO_WORLD_CONNECTOR_CLASS)
    }
    
    def "connectorHelloWorldArchive packages correct artifacts"() {
        when:
        buildFile << """
            repositories {
                mavenCentral()
            }
        """
        build("connectorHelloWorldArchive")
        def car = zipEntries("$distributions/connectorHelloWorld-${VERSION}-car.zip")
        
        then:
        car["lib/connectorHelloWorld-${VERSION}.jar"]
        car["lib/connector-sdk-util-${DEFAULT_SDK_VERSION}.jar"]
        car["META-INF/connector-descriptor.xml"].call() == resource(HELLO_WORLD_DESCRIPTOR)
        car["META-INF/connector-config.xml"].call() == generatedConfig(HELLO_WORLD_CONNECTOR_CLASS)
    }
    
    def "connectorSamples extracts samples source jar"() {
        def jar = getSourceJar(ConnectorArtifact.SDK_SAMPLES)
        File src = "$generatedSrc/connectorSamples/java" as File
        
        when:
        build("connectorSamples")
        
        then:
        src.exists()
        src.eachFileRecurse FileType.FILES, {
            assert it.text == jar[it.path - "${src.path}/"].call(), it.name
        }
    }
    
    def "sdk version is preferred for the api configuration"() {
        def version = "2.5.2"
        
        buildFile << """
            connector {
                sdkVersion "$version"
            }

            dependencies {
                api "${artifact.dependency()}"
            }
        """
        buildFile << sdkUtilDependency(version, "api")

        when:
        def result = build("dependencies")
        
        then:
        result.output.contains "${artifact.dependency()}:{prefer $version}"
        
        where:
        artifact << SDK_ARTIFACTS
    }
    
    def "sdk version is preferred for the connectorLib configuration"() {
        def version = "2.5.2"
        
        buildFile << """
            connector {
                sdkVersion "$version"
            }

            dependencies {
                connectorLib "${artifact.dependency()}"
            }
        """

        when:
        def result = build("dependencies")
        
        then:
        result.output.contains "${artifact.dependency()}:{prefer $version}"
        
        where:
        artifact << SDK_ARTIFACTS.findAll {!it.excluded}
    }
    
    def "sdk version is preferred for the implementation configuration"() {
        buildFile << """
            dependencies {
                implementation "${artifact.dependency()}"
            }
        """

        when:
        def result = build("dependencies")
        
        then:
        result.output.contains "${artifact.dependency(DEFAULT_SDK_VERSION)} (n)"
        result.output.contains "${artifact.dependency()}:{prefer $DEFAULT_SDK_VERSION}"
        
        where:
        artifact << (SDK_ARTIFACTS - ConnectorArtifact.SDK_API)
    }
    
    def "sdk version is preferred for custom configurations"() {
        def version = "2.5.2"
        
        buildFile << """
            connector {
                sdkVersion "$version"
            }

            configurations {
                customConf
            }

            dependencies {
                customConf "${artifact.dependency()}"
            }
        """

        when:
        def result = build("dependencies")
        
        then:
        result.output.contains "${artifact.dependency()}:{prefer $version}"
        
        where:
        artifact << SDK_ARTIFACTS
    }

	def "manifest includes default values"() {

		buildFile << """
			description = "the desc"	
		"""

		when:
		def manifest = loadManifest(connectorArchive())

		then:
		manifest["Manifest-Version"] == "1.0"
		manifest["connector-version"] == "$VERSION"
		manifest["connector-name"] == PROJECT
		manifest["connector-description"] == "the desc"
	}

	def "manifest supports custom values and entries"() {

		buildFile << """
			connectorArchive {
				manifest {
        			attributes("connector-version": 13, "connector-name": "secret", "connector-description": "custom description", "custom-property": "custom value")
				}
			}
		"""

		when:
		def manifest = loadManifest(connectorArchive())

		then:
		manifest["Manifest-Version"] == "1.0"
		manifest["connector-version"] == "13"
		manifest["connector-name"] == "secret"
		manifest["connector-description"] == "custom description"
		manifest["custom-property"] == "custom value"
	}


	//loads the manifest file from the car into a prop instance
	def loadManifest = { carFile ->
		def p = new Properties()
		p.load(new ByteArrayInputStream(carFile["META-INF/MANIFEST.MF"].call().bytes))
		p
	}
}
