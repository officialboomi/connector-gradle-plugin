// Copyright (c) 2021 Boomi, Inc.
package com.boomi.connector.gradle

import com.boomi.connector.gradle.util.OpenAPIDescriptorConstants
import groovy.json.JsonSlurper
import groovy.text.GStringTemplateEngine
import groovy.text.TemplateEngine
import groovy.xml.XmlSlurper
import org.gradle.internal.impldep.org.apache.commons.lang.SerializationUtils
import org.gradle.testkit.runner.TaskOutcome
import spock.lang.Unroll

/**
 * Functional specification for OpenAPI related tasks
 */
class OpenAPITasksFSpec extends BaseFSpec{
    
    /************ OpenAPI Groovy Input Template building constants  ****************/
    private static final String OPEN_API_CONNECTOR = "openAPIConnector"
    private static final String OPEN_API_SPECIFICATION = "openApiSpecification"
    private static final String SERVER = "server"
    private static final String COOKIE_SCOPE = "cookieScope"
    private static final String INCLUDE_PRIVATE_CERT = "includePrivateCert"
    private static final String INCLUDE_PUBLIC_CERT = "includePublicCert"
    private static final String OPERATIONS = "operations"
    private static final String HELP_TEXT = "helpText"
    private static final String LABEL = "label"
    private static final String ACCESS = "access"
    private static final String KEY_VALUE_PAIRS = "keyValuePairs"
    private static final String DEFAULT_VALUE = "defaultValue"
    private static final String INCLUDED_VALUES = "includedValues"
    private static final String DEFAULT_VALUE_COMMENTED = "// defaultValue"
    private static final String INCLUDED_VALUES_COMMENTED = "// includedValues"
    private static final String OPERATION_ID = "operationId"
    private static final String OPERATION_ID_CLOSURE = "operationId"
    private static final String METHOD = "method"
    private static final String PATH = "path"
    private static final String AUTH_TYPE = "authType"
    private static final String OAUTH2_CONFIG = "OAuth2Config"
    private static final String AUTHORIZATION_TOKEN_ENDPOINT = "authorizationTokenEndpoint"
    private static final String ACCESS_TOKEN_ENDPOINT = "accessTokenEndpoint"
    private static final String AUTHORIZATION_PARAMETERS = "authorizationParameters"
    private static final String ACCESS_TOKEN_PARAMETERS = "accessTokenParameters"
    private static final String SCOPE = "scope"
    private static final String GRANT_TYPE = "grantType"
    private static final String JWT_PARAMETERS = "jwtParameters"
    private static final String SIGNATURE_ALGORITHMS = "signatureAlgorithms"
    private static final String ISSUER = "issuer"
    private static final String SUBJECT = "subject"
    private static final String AUDIENCE = "audience"
    private static final String EXPIRATION = "expiration"
    private static final String SIGNATURE_KEY = "signatureKey"
    private static final String EXTENDED_CLAIMS = "extendedClaims"
    // set to [] for no operations. add a name in the list for additional operations
    private static final def OPERATION_NAMES = ["Create": ["Comment", "Issue"], "Update": ["FixVersion", "Assignee"]]
    
    //field values
    private static final String AUTH_TYPE_VALUE = """ "NONE" """
    private static final String OPEN_API_SPECIFICATION_VALUE = """ "/home/boomiuser/Documents/pagerDuty.json" """
    private static final String SERVER_VALUE = """ "https://api.sandbox.paypal.com" """
    private static final String COOKIE_SCOPE_VALUE = """ "GLOBAL" """
    private static final String INCLUDE_PRIVATE_CERT_VALUE = "true"
    private static final String INCLUDE_PUBLIC_CERT_VALUE = "true"
    private static final String LABEL_DEFAULT_VALUE = """ "Custom label here" """
    private static final String HELP_TEXT_DEFAULT_VALUE = """ "Custom helpText here" """
    private static final String DEFAULT_VALUE_DEFAULT = """ "Custom defaultValue here" """
    private static final String DEFAULT_VALUE_COOKIE_SCOPE = """ "GLOBAL" """
    private static final String DEFAULT_VALUE_AUTH_TYPE = """ "NONE" """
    private static final String DEFAULT_SIGNATURE_ALGORITHM_VALUE = """ "NONE" """
    private static final def INCLUDED_VALUES_DEFAULT_LIST = """ """
    private static final String METHOD_DEFAULT_VALUE = """ POST """
    private static final String PATH_DEFAULT_VALUE = """ some/valid/path/here """
    private static final String OPERATION_ID_VALUE = """ exampleStringOperationId """
    private static final String DEFAULT_ACCESS_VALUE = """ "enabled" """
    private static final String EXAMPLE_STRING_VALUE = """ "exampleStringValue" """
    private static final String DEFAULT_EXPIRATION_VALUE = """ "360" """
    private static final String DEFAULT_KEY_VALUE_PAIRS_VALUE = """  "parameterName1": "parameterValue1", "parameterName2": "parameterValue2" """
    private static final String DEFAULT_CLAIMS_KEY_VALUE_PAIRS_VALUE = """ "claimName1": "claimValue1", "claimName2": "claimValue2"  """
    private static final String DEFAULT_SCOPE_VALUE = """ "boomi.Read,boomi.Write" """
    private static final String DEFAULT_GRANT_TYPE_VALUE = """ "jwt-bearer" """
    private static final String DEFAULT_GRANT_TYPE_INCLUDED_VALUES = """ "jwt-bearer","client_credentials" """
    private static final String DEFAULT_SIGNATURE_ALGORITHMS_INCLUDED_VALUES = """ "NONE", "SHA256withRSA" """
    //control fields
    private static final Boolean isCOOKIE_SCOPE_CLOSURE = false
    private static final Boolean isINCLUDE_PRIVATE_CERT_CLOSURE = true
    private static final Boolean isINCLUDE_PUBLIC_CERT_CLOSURE = false
    private static final Boolean isAUTH_TYPE_CLOSURE = false
    private static final Boolean isAUTHORIZATION_TOKEN_ENDPOINT_CLOSURE = true
    private static final Boolean isACCESS_TOKEN_ENDPOINT_CLOSURE = true
    private static final Boolean isSCOPE_CLOSURE = true
    private static final Boolean isGRANT_TYPE_CLOSURE = true
    private static final Boolean isJWT_SIGNATURE_ALGORITHMS_CLOSURE = true
    private static final Boolean isJWT_ISSUER_CLOSURE = true
    private static final Boolean isJWT_SUBJECT_CLOSURE = true
    private static final Boolean isJWT_AUDIENCE_CLOSURE = true
    private static final Boolean isJWT_EXPIRATION_CLOSURE = true
    private static final Boolean isJWT_SIGNATURE_KEY_CLOSURE = true
    private static final Boolean isJWT_EXTENDED_CLAIMS_CLOSURE = true
    private static final Boolean SKIP_OPERATION_ID = false
    private static final Boolean SKIP_OPERATION_ID_CLOSURE = false
    private static final Boolean SKIP_OPERATIONS_BLOCK = false
    private static final Boolean SKIP_OPEN_API_SPECIFICATION_BLOCK = false
    private static final Boolean SKIP_SERVER_BLOCK = false
    private static final Boolean SKIP_PRIVATE_CERT_BLOCK = false
    private static final Boolean SKIP_PUBLIC_CERT_BLOCK = false
    private static final Boolean SKIP_COOKIE_SCOPE_BLOCK = false
    private static final Boolean SKIP_AUTH_TYPE_BLOCK = false
    private static final Boolean SKIP_OAUTH2_CONFIG_BLOCK = false
    private static final Boolean SKIP_JWT_PARAMETERS_BLOCK = false
    
    private static final def bindMap = [OPEN_API_CONNECTOR                          : OPEN_API_CONNECTOR,
                                        SERVER                                      : SERVER,
                                        SERVER_VALUE                                : SERVER_VALUE,
                                        SKIP_SERVER_BLOCK                           : SKIP_SERVER_BLOCK,
                                        SKIP_COOKIE_SCOPE_BLOCK                     : SKIP_COOKIE_SCOPE_BLOCK,
                                        COOKIE_SCOPE                                : COOKIE_SCOPE,
                                        AUTH_TYPE                                   : AUTH_TYPE,
                                        AUTH_TYPE_VALUE                             : AUTH_TYPE_VALUE,
                                        isAUTH_TYPE_CLOSURE                         : isAUTH_TYPE_CLOSURE,
                                        SKIP_AUTH_TYPE_BLOCK                        : SKIP_AUTH_TYPE_BLOCK,
                                        isCOOKIE_SCOPE_CLOSURE                      : isCOOKIE_SCOPE_CLOSURE,
                                        COOKIE_SCOPE_VALUE                          : COOKIE_SCOPE_VALUE,
                                        LABEL                                       : LABEL,
                                        LABEL_DEFAULT_VALUE                         : LABEL_DEFAULT_VALUE,
                                        HELP_TEXT                                   : HELP_TEXT,
                                        HELP_TEXT_DEFAULT_VALUE                     : HELP_TEXT_DEFAULT_VALUE,
                                        DEFAULT_VALUE                               : DEFAULT_VALUE,
                                        DEFAULT_VALUE_AUTH_TYPE                     : DEFAULT_VALUE_AUTH_TYPE,
                                        DEFAULT_VALUE_COOKIE_SCOPE                  : DEFAULT_VALUE_COOKIE_SCOPE,
                                        INCLUDED_VALUES                             : INCLUDED_VALUES,
                                        INCLUDED_VALUES_DEFAULT_LIST                : INCLUDED_VALUES_DEFAULT_LIST,
                                        SKIP_OPEN_API_SPECIFICATION_BLOCK           : SKIP_OPEN_API_SPECIFICATION_BLOCK,
                                        OPEN_API_SPECIFICATION                      : OPEN_API_SPECIFICATION,
                                        OPEN_API_SPECIFICATION_VALUE                : OPEN_API_SPECIFICATION_VALUE,
                                        SKIP_PRIVATE_CERT_BLOCK                     : SKIP_PRIVATE_CERT_BLOCK,
                                        INCLUDE_PRIVATE_CERT                        : INCLUDE_PRIVATE_CERT,
                                        isINCLUDE_PRIVATE_CERT_CLOSURE              : isINCLUDE_PRIVATE_CERT_CLOSURE,
                                        DEFAULT_VALUE_COMMENTED                     : DEFAULT_VALUE_COMMENTED,
                                        DEFAULT_VALUE_DEFAULT                       : DEFAULT_VALUE_DEFAULT,
                                        INCLUDED_VALUES_COMMENTED                   : INCLUDED_VALUES_COMMENTED,
                                        INCLUDE_PRIVATE_CERT_VALUE                  : INCLUDE_PRIVATE_CERT_VALUE,
                                        SKIP_PUBLIC_CERT_BLOCK                      : SKIP_PUBLIC_CERT_BLOCK,
                                        INCLUDE_PUBLIC_CERT                         : INCLUDE_PUBLIC_CERT,
                                        isINCLUDE_PUBLIC_CERT_CLOSURE               : isINCLUDE_PUBLIC_CERT_CLOSURE,
                                        INCLUDE_PUBLIC_CERT_VALUE                   : INCLUDE_PUBLIC_CERT_VALUE,
                                        SKIP_OPERATIONS_BLOCK                       : SKIP_OPERATIONS_BLOCK,
                                        OPERATIONS                                  : OPERATIONS,
                                        OAUTH2_CONFIG                               : OAUTH2_CONFIG,
                                        SKIP_OAUTH2_CONFIG_BLOCK                    : SKIP_OAUTH2_CONFIG_BLOCK,
                                        ACCESS                                      : ACCESS,
                                        KEY_VALUE_PAIRS                             : KEY_VALUE_PAIRS,
                                        AUTHORIZATION_TOKEN_ENDPOINT                : AUTHORIZATION_TOKEN_ENDPOINT,
                                        ACCESS_TOKEN_ENDPOINT                       : ACCESS_TOKEN_ENDPOINT,
                                        AUTHORIZATION_PARAMETERS                    : AUTHORIZATION_PARAMETERS,
                                        ACCESS_TOKEN_PARAMETERS                     : ACCESS_TOKEN_PARAMETERS,
                                        SCOPE                                       : SCOPE,
                                        GRANT_TYPE                                  : GRANT_TYPE,
                                        JWT_PARAMETERS                              : JWT_PARAMETERS,
                                        SIGNATURE_ALGORITHMS                        : SIGNATURE_ALGORITHMS,
                                        ISSUER                                      : ISSUER,
                                        SUBJECT                                     : SUBJECT,
                                        AUDIENCE                                    : AUDIENCE,
                                        EXPIRATION                                  : EXPIRATION,
                                        SIGNATURE_KEY                               : SIGNATURE_KEY,
                                        EXTENDED_CLAIMS                             : EXTENDED_CLAIMS,
                                        DEFAULT_ACCESS_VALUE                        : DEFAULT_ACCESS_VALUE,
                                        EXAMPLE_STRING_VALUE                        : EXAMPLE_STRING_VALUE,
                                        DEFAULT_EXPIRATION_VALUE                    : DEFAULT_EXPIRATION_VALUE,
                                        DEFAULT_KEY_VALUE_PAIRS_VALUE               : DEFAULT_KEY_VALUE_PAIRS_VALUE,
                                        DEFAULT_SCOPE_VALUE                         : DEFAULT_SCOPE_VALUE,
                                        DEFAULT_GRANT_TYPE_VALUE                    : DEFAULT_GRANT_TYPE_VALUE,
                                        isAUTHORIZATION_TOKEN_ENDPOINT_CLOSURE      : isAUTHORIZATION_TOKEN_ENDPOINT_CLOSURE,
                                        isACCESS_TOKEN_ENDPOINT_CLOSURE             : isACCESS_TOKEN_ENDPOINT_CLOSURE,
                                        isSCOPE_CLOSURE                             : isSCOPE_CLOSURE,
                                        isGRANT_TYPE_CLOSURE                        : isGRANT_TYPE_CLOSURE,
                                        isJWT_SIGNATURE_ALGORITHMS_CLOSURE          : isJWT_SIGNATURE_ALGORITHMS_CLOSURE,
                                        isJWT_ISSUER_CLOSURE                        : isJWT_ISSUER_CLOSURE,
                                        isJWT_SUBJECT_CLOSURE                       : isJWT_SUBJECT_CLOSURE,
                                        isJWT_AUDIENCE_CLOSURE                      : isJWT_AUDIENCE_CLOSURE,
                                        isJWT_EXPIRATION_CLOSURE                    : isJWT_EXPIRATION_CLOSURE,
                                        isJWT_SIGNATURE_KEY_CLOSURE                 : isJWT_SIGNATURE_KEY_CLOSURE,
                                        isJWT_EXTENDED_CLAIMS_CLOSURE               : isJWT_EXTENDED_CLAIMS_CLOSURE,
                                        DEFAULT_GRANT_TYPE_INCLUDED_VALUES          : DEFAULT_GRANT_TYPE_INCLUDED_VALUES,
                                        DEFAULT_SIGNATURE_ALGORITHMS_INCLUDED_VALUES: DEFAULT_SIGNATURE_ALGORITHMS_INCLUDED_VALUES,
                                        SKIP_JWT_PARAMETERS_BLOCK                   : SKIP_JWT_PARAMETERS_BLOCK,
                                        DEFAULT_CLAIMS_KEY_VALUE_PAIRS_VALUE        : DEFAULT_CLAIMS_KEY_VALUE_PAIRS_VALUE,
                                        DEFAULT_SIGNATURE_ALGORITHM_VALUE           : DEFAULT_SIGNATURE_ALGORITHM_VALUE,
                                        OPERATION_PARAM_MAP                         : [OPERATION_NAMES: OPERATION_NAMES,
                                                                            SKIP_OPERATION_ID        : SKIP_OPERATION_ID,
                                                                            OPERATION_ID             : OPERATION_ID,
                                                                            OPERATION_ID_VALUE       : OPERATION_ID_VALUE,
                                                                            SKIP_OPERATION_ID_CLOSURE: SKIP_OPERATION_ID_CLOSURE,
                                                                            OPERATION_ID_CLOSURE     : OPERATION_ID_CLOSURE,
                                                                            METHOD                   : METHOD,
                                                                            METHOD_DEFAULT_VALUE     : METHOD_DEFAULT_VALUE,
                                                                            PATH                     : PATH,
                                                                            PATH_DEFAULT_VALUE       : PATH_DEFAULT_VALUE]
                                        ]
    File templateFile
    File openAPISpecTemplateFile
    File serverTemplateFile
    File cookieScopeTemplateFile
    File certFieldsTemplateFile
    File authTypeTemplateFile
    File openAPIOperationsTemplateFile
    File oAuth2ConfigTemplateFile
    List<File> templates
    TemplateEngine engine
    
    def setup(){
        // templateFile = new File('src/functionalTest/resources/groovyInputTemplate.template')
        projectDir.newFile("gradle.properties") << "enableConnectorPluginOpenAPITasks = true"
        openAPISpecTemplateFile = new File('src/functionalTest/resources/openAPISpecConnectionField.template')
        serverTemplateFile = new File('src/functionalTest/resources/serverConnectionField.template')
        cookieScopeTemplateFile = new File('src/functionalTest/resources/cookieScopeConnectionField.template')
        certFieldsTemplateFile = new File('src/functionalTest/resources/certConnectionFields.template')
        authTypeTemplateFile = new File('src/functionalTest/resources/authTypeConnectionField.template')
        oAuth2ConfigTemplateFile = new File('src/functionalTest/resources/oauth2ConfigConnectionField.template')
        openAPIOperationsTemplateFile = new File('src/functionalTest/resources/openAPIOperations.template')
        templates = [openAPISpecTemplateFile, serverTemplateFile, cookieScopeTemplateFile, certFieldsTemplateFile, authTypeTemplateFile,
                     oAuth2ConfigTemplateFile, openAPIOperationsTemplateFile]
        engine = new GStringTemplateEngine()
    }
    
    def "connectorJsonConfig generates resource file"() {
        def task = ":connectorJsonConfig"
        File resourceFile = "$buildDir/resources/main/META-INF/openapi-config.json" as File
        
        buildFile << buildGroovyInputFromTemplate(null)
        
        when:
        build(task)
        
        then:
        resourceFile.exists()
    }
    
    def "connectorJsonConfig requires openAPIConnector script block"() {
        def task = ":connectorJsonConfig"
        def inputsToChange = [OPEN_API_CONNECTOR: "notOpenAPIConnector"]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        build(task).task(task)
        
        then:
        thrown(Exception)
    }
    
    def "connectorJsonConfig generates single operation when build has duplicate actionTypes"() {
        def task = ":connectorJsonConfig"
        File resourceFile = "$buildDir/resources/main/META-INF/openapi-config.json" as File
        // groovy input with duplicate actionType "Comment"
        buildFile << """openAPIConnector {

                        openApiSpecification "/home/boomiuser/Documents/pagerDuty.json"
                        server "https://api.sandbox.paypal.com"
                        authType ""
                        operations {
                            "Create" {
                                "Batman" {
                                    operationId "adopted_darkness"
                                }
                            }
                            "Create" {
                                "Bane" {
                                    operationId "born_in_darkness"
                                }
                            }
                        }
                    }
        """
        when:
        build(task).task(task)
        def json = new JsonSlurper().parse(resourceFile)
        def create = json.operations.find { it.name == "Create" }
        def darknessPeople = create?.operationObjects

        then:
        create && darknessPeople
        json.operations.size() == 1
        darknessPeople.size() == 2
        darknessPeople.find { it.schemaName == "Batman" }
        darknessPeople.find { it.schemaName == "Bane" }
    }
    
    def "connectorJsonConfig expects operationId for all actionTypes"() {
        def task = ":connectorJsonConfig"
        def inputsToChange = [SKIP_OPERATION_ID: true]
        
        //groovy input with no operationId for "Comment" actionType
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        build(task).task(task)
        
        then:
        thrown(Exception)
    }
    
    def "connectorJsonConfig expects object operationId for actionType"() {
        def task = ":connectorJsonConfig"
        def inputsToChange = [SKIP_OPERATION_ID_CLOSURE: true]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        build(task).task(task)
        
        then:
        thrown(Exception)
    }
    
    def "connectorJsonConfig expects method and path under operationId"() {
        def task = ":connectorJsonConfig"
        def inputsToChange = [METHOD: "// method"]
        
        //groovy input with either 'method' or 'path' unavailable under operationId
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        build(task).task(task)
        
        then:
        thrown(Exception)
    }
    
    def "connectorJsonConfig requires operations keyword"() {
        def task = ":connectorJsonConfig"
        def inputsToChange = [SKIP_OPERATIONS_BLOCK: true]
        
        //groovy input with no operations keyword
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorJsonConfig requires atleast one operation defined"() {
        def task = ":connectorJsonConfig"
        def inputsToChange = [OPERATION_NAMES: [:]]
        
        // groovy input with no operation definition
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorJsonConfig requires openApiSpecification field defined"() {
        def task = ":connectorJsonConfig"
        def inputsToChange = [SKIP_OPEN_API_SPECIFICATION_BLOCK: true]
        
        // groovy input with no openApiSpecification field
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor generates descriptor file"() {
        def task = ":connectorDescriptor"
        File resourceFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        // groovy input
        buildFile << buildGroovyInputFromTemplate(null)
        
        when:
        build(task)
        
        then:
        resourceFile.exists()
    }
    
    def "connectorDescriptor generates descriptor having correct # of operations"() {
        def task = ":connectorDescriptor"
        File resourceFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        // groovy input with 2 operations
        buildFile << buildGroovyInputFromTemplate(null)
        
        when:
        build(task)
        
        then:
        countMatches(text(resourceFile), "<operation") == 2
    }
    
    def "connectorDescriptor handles duplicate operations"() {
        def task = ":connectorDescriptor"
        File resourceFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        def inputsToChange = [OPERATION_NAMES: ["Create": ["Comment", "Issue"], "Create": ["FixVersion", "Assignee"]]]
        
        // groovy input with 2 operations with same name
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        build(task)
        
        then:
        countMatches(text(resourceFile), "<operation") == 1
    }
    
    def "connectorDescriptor requires operations keyword"() {
        def task = ":connectorDescriptor"
        def inputsToChange = [SKIP_OPERATIONS_BLOCK: true]
        
        // groovy input with no operation definition
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor requires atleast one operation defined"() {
        def task = ":connectorDescriptor"
        def inputsToChange = [OPERATION_NAMES: [:]]
        
        // groovy input with no operation definition
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor requires atleast one operationObject under each operation defined"() {
        def task = ":connectorDescriptor"
        def inputsToChange = [OPERATION_NAMES: ["Create": ["Comment", "Issue"], "Update": []]]
        
        // groovy input with no operationObject definition under 'Update'
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor generated descriptor has correct # of connection fields"() {
        def task = ":connectorDescriptor"
        File resourceFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        // groovy input with 4 expected connection fields
        buildFile << buildGroovyInputFromTemplate(null)
        
        when:
        build(task)
        
        then:
        countMatches(text(resourceFile), "<field") == 5
    }
    
    def "connectorDescriptor generated descriptor has server field"() {
        def task = ":connectorDescriptor"
        File resourceFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [SKIP_SERVER_BLOCK: true]
        
        // groovy input with 'server' field commented out.
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        build(task)
        
        then:
        text(resourceFile).contains("id=\"url\" label=\"Server\"")
    }
    
    def "connectorDescriptor throws error for improper value of cookieScope"() {
        def task = ":connectorDescriptor"
        def inputsToChange = [isCOOKIE_SCOPE_CLOSURE: false,
                              COOKIE_SCOPE_VALUE    : """ "NOT_GLOBAL" """]
        
        // groovy input with improper value for cookieScope field
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor throws error if openApiSpecification value is a closure"() {
        def task = ":connectorDescriptor"
        def inputsToChange = [OPEN_API_SPECIFICATION_VALUE: """ {defaultValue "/home/boomiuser/Documents/pagerDuty.json"} """]
        
        // groovy input with closure value for openApiSpecification field
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        build(task).task(task)
        
        then:
        thrown(Exception)
    }
    
    def "connectorDescriptor throws error if server value is a closure"() {
        def task = ":connectorDescriptor"
        def inputsToChange = [SERVER_VALUE: """ {defaultValue "https://api.sandbox.paypal.com"} """]
        
        // groovy input with closure value for server field
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        build(task).task(task)
        
        then:
        thrown(Exception)
    }
    
    def "connectorDescriptor sets string value for cookieScope as defaultValue"() {
        def task = ":connectorDescriptor"
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        buildFile << buildGroovyInputFromTemplate(null)
        
        when:
        build(task)
        
        then:
        text(descriptorFile).contains("<defaultValue>GLOBAL</defaultValue>")
    }
    
    def "connectorDescriptor accepts closure value for cookieScope"() {
        def task = ":connectorDescriptor"
        
        buildFile << buildGroovyInputFromTemplate(null)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
    }
    
    def "connectorDescriptor throws errors when defaultValue is invalid for cookieScope"() {
        def task = ":connectorDescriptor"
        def inputsToChange = [isCOOKIE_SCOPE_CLOSURE    : true,
                              DEFAULT_VALUE_COOKIE_SCOPE: """ "NOT_GLOBAL" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor throws errors when includedValues contains invalid value for cookieScope"() {
        def task = ":connectorDescriptor"
        def inputsToChange = [isCOOKIE_SCOPE_CLOSURE      : true,
                              INCLUDED_VALUES_DEFAULT_LIST: """ "NOT_GLOBAL", "CONNECTOR_SHAPE" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor generates dropdown options mentioned under includedValues for cookieScope"() {
        def task = ":connectorDescriptor"
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        def inputsToChange = [isCOOKIE_SCOPE_CLOSURE      : true,
                              INCLUDED_VALUES_DEFAULT_LIST: """ "GLOBAL", "CONNECTOR_SHAPE", "IGNORED" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        countMatches(text(descriptorFile), '<allowedValue label="GLOBAL">') == 1
        countMatches(text(descriptorFile), '<allowedValue label="CONNECTOR_SHAPE">') == 1
        countMatches(text(descriptorFile), '<allowedValue label="IGNORED">') == 1
    }
    
    def "connectorDescriptor accepts boolean for includePrivateCert and includePublicCert fields"() {
        def task = ":connectorDescriptor"
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        buildFile << buildGroovyInputFromTemplate(null)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("id=\"publicCertificate\"")
        text(descriptorFile).contains("id=\"privateCertificate\"")
    }
    
    def "connectorDescriptor accepts closure value for cert fields"() {
        def task = ":connectorDescriptor"
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        def inputsToChange = [isINCLUDE_PRIVATE_CERT_CLOSURE: true]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("id=\"publicCertificate\"")
        text(descriptorFile).contains("id=\"privateCertificate\"")
    }
    
    def "connectorDescriptor neglects defaultValue and includedValues for cert fields if included"() {
        def task = ":connectorDescriptor"
        
        def inputsToChange = [DEFAULT_VALUE_COMMENTED  : "defaultValue",
                              INCLUDED_VALUES_COMMENTED: "includedValues"]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
    }
    
    def "connectorDescriptor throws error when authType field is missing"() {
        def task = ":connectorDescriptor"
        
        def inputsToChange = [SKIP_AUTH_TYPE_BLOCK: true]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor accepts string value for authType field"() {
        def task = ":connectorDescriptor"
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        buildFile << buildGroovyInputFromTemplate(null)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<field id=\"auth\"")
    }
    
    def "connectorDescriptor throws error for improper string value for authType field"() {
        def task = ":connectorDescriptor"
        
        def inputsToChange = [AUTH_TYPE_VALUE: """ "AWS" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor generates dependent connection fields when authType string value is BASIC"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [AUTH_TYPE_VALUE: """ "BASIC" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<field id=\"auth\"")
        text(descriptorFile).contains("<field id=\"username\"")
        text(descriptorFile).contains("<field id=\"password\"")
        text(descriptorFile).contains("<field id=\"preemptive\"")
    }
    
    def "connectorDescriptor generates dependent connection fields when authType string value is CUSTOM"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [AUTH_TYPE_VALUE: """ "CUSTOM" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<field id=\"auth\"")
        text(descriptorFile).contains("<field id=\"customAuthCredentials\"")
    }

    private def xmlParser(File xmlFile) {
        return new XmlSlurper().parse(xmlFile)
    }

    private def findFieldByType(file, fieldType) {
        return xmlParser(file).(OpenAPIDescriptorConstants.FIELD_ELEMENT)
                .find {it.@type == "$fieldType"}
    }

    def "connectorDescriptor generates scope default value"() {
        def task = ":connectorDescriptor"
        def newScope = "down periscope"

        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File

        def inputsToChange = [
                isSCOPE_CLOSURE: true,
                DEFAULT_SCOPE_VALUE: """ "$newScope" """,
                isAUTH_TYPE_CLOSURE: true,
                DEFAULT_VALUE_AUTH_TYPE: """ "OAUTH2" """]

        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        text(buildFile)
        when:
        def result = build(task).task(task)

        then:
        result?.outcome == TaskOutcome.SUCCESS
        findFieldByType(descriptorFile, OpenAPIDescriptorConstants.TYPE_OAUTH)
                ."$OpenAPIDescriptorConstants.OAUTH_CONFIG_ELEMENT"
                ."$OpenAPIDescriptorConstants.SCOPE_ELEMENT"
                ."$OpenAPIDescriptorConstants.DEFAULT_VALUE"
                .text() == "$newScope"
    }

    def "connectorDescriptor generates dependent connection fields when authType string value is DIGEST"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [AUTH_TYPE_VALUE: """ "DIGEST" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<field id=\"auth\"")
        text(descriptorFile).contains("<field id=\"username\"")
        text(descriptorFile).contains("<field id=\"password\"")
    }
    
    def "connectorDescriptor generates dependent connection fields when authType string value is OAUTH"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [AUTH_TYPE_VALUE: """ "OAUTH2" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<field id=\"auth\"")
        text(descriptorFile).contains("<field id=\"oauthContext\"")
        text(descriptorFile).contains("<field id=\"preemptive\"")
    }
    
    def "connectorDescriptor accepts closure value for authType field"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [isAUTH_TYPE_CLOSURE: true]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<field id=\"auth\"")
    }
    
    def "connectorDescriptor throws error for improper defaultValue of authType field"() {
        def task = ":connectorDescriptor"
        
        def inputsToChange = [isAUTH_TYPE_CLOSURE: true, DEFAULT_VALUE_AUTH_TYPE: """ "AWS" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor throws error for improper includedValues of authType field"() {
        def task = ":connectorDescriptor"
        
        def inputsToChange = [isAUTH_TYPE_CLOSURE: true, INCLUDED_VALUES_DEFAULT_LIST: """ "NOAUTH" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor throws error if neither includedValues nor defaultValue of authType field is defined"() {
        def task = ":connectorDescriptor"
        
        def inputsToChange = [isAUTH_TYPE_CLOSURE: true, INCLUDED_VALUES_DEFAULT_LIST: """ """, DEFAULT_VALUE_AUTH_TYPE: """ "" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor generates dependent connection fields when authType closure value includes BASIC"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [isAUTH_TYPE_CLOSURE: true, DEFAULT_VALUE_AUTH_TYPE: """ "BASIC" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<field id=\"auth\"")
        text(descriptorFile).contains("<field id=\"username\"")
        text(descriptorFile).contains("<field id=\"password\"")
        text(descriptorFile).contains("<field id=\"preemptive\"")
    }
    
    def "connectorDescriptor generates dependent connection fields when authType closure value includes CUSTOM"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [isAUTH_TYPE_CLOSURE: true, DEFAULT_VALUE_AUTH_TYPE: """ "CUSTOM" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<field id=\"auth\"")
        text(descriptorFile).contains("<field id=\"customAuthCredentials\"")
    }
    
    def "connectorDescriptor generates dependent connection fields when authType closure value includes DIGEST"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [isAUTH_TYPE_CLOSURE: true, DEFAULT_VALUE_AUTH_TYPE: """ "DIGEST" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<field id=\"auth\"")
        text(descriptorFile).contains("<field id=\"username\"")
        text(descriptorFile).contains("<field id=\"password\"")
    }
    
    def "connectorDescriptor generates dependent connection fields when authType closure value includes OAUTH"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [isAUTH_TYPE_CLOSURE: true, DEFAULT_VALUE_AUTH_TYPE: """ "OAUTH2" """]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = build(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<field id=\"auth\"")
        text(descriptorFile).contains("<field id=\"oauthContext\"")
        text(descriptorFile).contains("<field id=\"preemptive\"")
    }
    
    def "connectorDescriptor generates oauth2FieldConfig elements when OAUTH2Config closure exists and when authType equals OAUTH2"(){
        def task = ":connectorDescriptor"
    
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
    
        def inputsToChange = [isAUTH_TYPE_CLOSURE: true, DEFAULT_VALUE_AUTH_TYPE: """ "OAUTH2" """]
    
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
    
        when:
        def result = build(task).task(task)
    
        then:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains("<oauth2FieldConfig>")
        text(descriptorFile).contains("</oauth2FieldConfig>")
    }
    
    def "connectorDescriptor throws error if incorrect access value is assigned for any OAUTH2 subfield"(){
        def task = ":connectorDescriptor"
       
        def inputsToChange = [DEFAULT_ACCESS_VALUE:""" "invalidValue" """ , DEFAULT_VALUE_AUTH_TYPE: """ "OAUTH2" """, isAUTH_TYPE_CLOSURE: true]
    
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
    
        when:
        def result = buildAndFail(task).task(task)
    
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor throws error if grantType OAUTH2 subfield value is incorrect"(){
        def task = ":connectorDescriptor"
      
        def inputsToChange = [DEFAULT_GRANT_TYPE_VALUE:""" "invalidValue" """ , DEFAULT_VALUE_AUTH_TYPE: """ "OAUTH2" """, isAUTH_TYPE_CLOSURE: true]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    
    def "connectorDescriptor throws error if grantType OAUTH2 subfield value is jwt-bearer and jwtParameters closure isn't defined"(){
        def task = ":connectorDescriptor"

        def inputsToChange = [SKIP_JWT_PARAMETERS_BLOCK: true, DEFAULT_VALUE_AUTH_TYPE: """ "OAUTH2" """, isAUTH_TYPE_CLOSURE: true]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)

        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }

    def "grant type is defaulted authcode and doesn't include jwt-bearer so ensure build success when jwt is omitted"(){
        def task = ":connectorDescriptor"
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File

        def inputsToChange = [SKIP_JWT_PARAMETERS_BLOCK: true, //omit jwtParam closure
                              DEFAULT_VALUE_AUTH_TYPE: """ "OAUTH2" """,
                              isAUTH_TYPE_CLOSURE: true,
                              DEFAULT_GRANT_TYPE_VALUE:""" "code" """, //jwt-bearer not a grant type option
                              DEFAULT_GRANT_TYPE_INCLUDED_VALUES: """ "code","client_credentials" """
        ]

        buildFile << buildGroovyInputFromTemplate(inputsToChange)

        when:
        def result = build(task).task(task)

        then:
        result?.outcome == TaskOutcome.SUCCESS
        !text(descriptorFile).contains("jwtParameters>")
    }

    def "grant type is default to jwt so ensure failure when jwt is not present in build"(){
        def task = ":connectorDescriptor"
        def inputsToChange = [SKIP_JWT_PARAMETERS_BLOCK: true,
                              DEFAULT_VALUE_AUTH_TYPE: """ "OAUTH2" """,
                              isAUTH_TYPE_CLOSURE: true,
                              DEFAULT_GRANT_TYPE_VALUE:""" "jwt-bearer" """,
                              DEFAULT_GRANT_TYPE_INCLUDED_VALUES: """ "code","client_credentials" """
        ]

        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        def result = buildAndFail(task)

        when:
        def taskStatus = result.task(task)

        then:
        taskStatus?.outcome == TaskOutcome.FAILED
        result?.getOutput()?.contains("jwtParameters is required")
    }

    def "connectorDescriptor throws error if includedValues list is defined improperly"(){
        def task = ":connectorDescriptor"
        
        def inputsToChange = [DEFAULT_GRANT_TYPE_INCLUDED_VALUES: """ "not:list", "map:instead" """, DEFAULT_VALUE_AUTH_TYPE: """ "OAUTH2" """,
                              isAUTH_TYPE_CLOSURE: true]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        result?.outcome == TaskOutcome.FAILED
    }
    def "connectorDescriptor throws error if keyValuePairs is defined improperly"(){
        def task = ":connectorDescriptor"
        
        def inputsToChange = [DEFAULT_KEY_VALUE_PAIRS_VALUE: """ "not", "map", "list","instead" """, DEFAULT_VALUE_AUTH_TYPE: """ "OAUTH2" """,
                              isAUTH_TYPE_CLOSURE: true]
        
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        
        when:
        def result = buildAndFail(task).task(task)
        
        then:
        !result?.outcome
        
    }
    def "connectorDescriptor accepts String input value for OAuth2Config subfields"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [(isSubFieldClosure) : false, AUTH_TYPE_VALUE: """ "OAUTH2" """]
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        def result = build(task).task(task)
        
        expect:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains(xmlElement)
        
        where:
        isSubFieldClosure                             | xmlElement
        'isAUTHORIZATION_TOKEN_ENDPOINT_CLOSURE'      | "</authorizationTokenEndpoint>"
        'isACCESS_TOKEN_ENDPOINT_CLOSURE'             | "</accessTokenEndpoint>"
        'isSCOPE_CLOSURE'                             | "</scope>"
        'isGRANT_TYPE_CLOSURE'                        | "</grantType>"
        'isJWT_SIGNATURE_ALGORITHMS_CLOSURE'          | "</signatureAlgorithms>"
        'isJWT_ISSUER_CLOSURE'                        | "</issuer>"
        'isJWT_SUBJECT_CLOSURE'                       | "</subject>"
        'isJWT_AUDIENCE_CLOSURE'                      | "</audience>"
        'isJWT_EXPIRATION_CLOSURE'                    | "</expiration>"
    }
    
    def "connectorDescriptor accepts closure input value for OAuth2Config subfields"() {
        def task = ":connectorDescriptor"
        
        File descriptorFile = "$buildDir/resources/main/META-INF/connector-descriptor.xml" as File
        
        def inputsToChange = [(isSubFieldClosure) : true, AUTH_TYPE_VALUE: """ "OAUTH2" """]
        buildFile << buildGroovyInputFromTemplate(inputsToChange)
        def result = build(task).task(task)
        
        expect:
        result?.outcome == TaskOutcome.SUCCESS
        text(descriptorFile).contains(xmlElement)
        
        where:
        isSubFieldClosure                             | xmlElement
        'isAUTHORIZATION_TOKEN_ENDPOINT_CLOSURE'      | "</authorizationTokenEndpoint>"
        'isACCESS_TOKEN_ENDPOINT_CLOSURE'             | "</accessTokenEndpoint>"
        'isSCOPE_CLOSURE'                             | "</scope>"
        'isGRANT_TYPE_CLOSURE'                        | "</grantType>"
        'isJWT_SIGNATURE_ALGORITHMS_CLOSURE'          | "</signatureAlgorithms>"
        'isJWT_ISSUER_CLOSURE'                        | "</issuer>"
        'isJWT_SUBJECT_CLOSURE'                       | "</subject>"
        'isJWT_AUDIENCE_CLOSURE'                      | "</audience>"
        'isJWT_EXPIRATION_CLOSURE'                    | "</expiration>"
        'isJWT_SIGNATURE_KEY_CLOSURE'                 | "</signatureKey>"
    }
    
    def "connectorArchive task depends on connectorJsonConfig task"() {
        buildFile << buildGroovyInputFromTemplate(null)
        when:
        def carTask = build("connectorArchive").task(":connectorJsonConfig")
        
        then:
        carTask?.outcome == TaskOutcome.SUCCESS
    }
    
    def "connectorArchive task depends on connectorDescriptor task"() {
        buildFile << buildGroovyInputFromTemplate(null)
        when:
        def carTask = build("connectorArchive").task(":connectorDescriptor")
        
        then:
        carTask?.outcome == TaskOutcome.SUCCESS
    }
    
    def "connectorArchive packages generated connector config with OpenAPIDslConnector className"() {
        
        def className = "com.boomi.connector.openapi.dsl.OpenAPIDslConnector"
        buildFile << buildGroovyInputFromTemplate(null)
        
        when:
        def carFile = connectorArchive()
        
        then:
        carFile["META-INF/connector-config.xml"]?.call() == generatedConfig(className)
    }

    @Unroll("#[#iterationIndex] 'META-INF/#fileName' -> plugin will override: #willBeOverridden")
    def "openAPI resources override explicit resources set by user"() {
        def project = new FileTreeBuilder(projectDir.root)

        project.src {
            "main" {
                "resources" {
                    "META-INF" {
                        "${fileName}"(fileContent)
                    }
                }
            }
        }

        buildFile << buildGroovyInputFromTemplate(null)

        when:
        def carFile = connectorArchive()

        then:
        def result = carFile["META-INF/${fileName}"].call()
        assert willBeOverridden ? result != fileContent : result == fileContent

        where:
        fileName                    | fileContent                       | willBeOverridden
        "packaged"                  | "yes"                             | false
        "some-other-file.txt"       | "Hello World!"                    | false
        "connector-descriptor.xml"  | "<GenericConnectorDescriptor/>"   | false
        "connector-config.xml"      | "<GenericConnector/>"             | false
        "openapi-config.json"       | "{}"                              | false
    }

    //********************* Groovy Input Template Helper methods ****************************//
    
    static def getOperationDefinition(Map operationParamMap) {
        def definitionType = [getOperationIdDefinition(operationParamMap), getOperationIdClosureDefinition(operationParamMap)]
        def operationDefinition = new StringBuilder("")
        
        if (operationParamMap.get("OPERATION_NAMES")) {
            operationParamMap.get("OPERATION_NAMES").each {oprName, operationObjectsList ->
                def operationObjects = [:]
                operationObjectsList.eachWithIndex {item, i -> operationObjects << ["${item}": definitionType[i % 2]]
                }
                operationDefinition << createOperation(oprName, operationObjects).toString()
            }
            return """ { $operationDefinition } """
        }
        return """{ }"""
    }
    
    static def getOperationIdDefinition(Map operationParamMap) {
        if (operationParamMap.get("SKIP_OPERATION_ID")) {
            return """{ }"""
        }
        return """ {
            ${operationParamMap.get("OPERATION_ID")} "${operationParamMap.get("OPERATION_ID_VALUE")}"
        }
        """
    }
    
    static def getOperationIdClosureDefinition(Map operationParamMap) {
        if (operationParamMap.get("SKIP_OPERATION_ID_CLOSURE")) {
            return """{ }"""
        }
        return """ {
            ${operationParamMap.get("OPERATION_ID_CLOSURE")} {
                    ${operationParamMap.get("METHOD")} "${operationParamMap.get("METHOD_DEFAULT_VALUE")}"
                    ${operationParamMap.get("PATH")}   "${operationParamMap.get("PATH_DEFAULT_VALUE")}"
            }
        }
        """
    }
    
    static def createOperation(operationName, operationObjectMap) {
        StringBuilder operationObjects = new StringBuilder(""" \n\t\t\t\t"${operationName}"{""")
        operationObjectMap.each {
            operationObjects.append(""" \n\t\t\t\t\t"$it.key" $it.value""")
        }
        return operationObjects.append("\t\t\t\t\t}")
    }
    
    def buildGroovyInputFromTemplate(Map inputsToChange) {
        //creating a deep copy of default inputs
        Map bindMapCopy = SerializationUtils.clone(bindMap) as Map
        StringBuilder groovyInput = new StringBuilder("")
        if (inputsToChange) {
            inputsToChange.each {key, value ->
                if (bindMapCopy.get(key) == null) {
                    if (bindMapCopy.OPERATION_PARAM_MAP.get(key) != null) {
                        bindMapCopy.OPERATION_PARAM_MAP.put(key, value)
                    }
                } else {
                    bindMapCopy.put(key, value)
                }
            }
        }
        templates.each {file ->
            groovyInput.append(engine.createTemplate(file).make(bindMapCopy).toString())
        }
        return groovyInput
    }
}
