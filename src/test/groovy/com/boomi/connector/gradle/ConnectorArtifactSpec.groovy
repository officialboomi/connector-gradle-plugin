// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.gradle

import spock.lang.Specification

import static com.boomi.connector.gradle.ConnectorArtifact.BOOMI_JSON_UTIL
import static com.boomi.connector.gradle.ConnectorArtifact.BOOMI_UTIL
import static com.boomi.connector.gradle.ConnectorArtifact.COMMON_LITE
import static com.boomi.connector.gradle.ConnectorArtifact.COMMON_SDK
import static com.boomi.connector.gradle.ConnectorArtifact.SDK_API
import static com.boomi.connector.gradle.ConnectorArtifact.SDK_MODEL
import static com.boomi.connector.gradle.ConnectorArtifact.SDK_OPEN_API
import static com.boomi.connector.gradle.ConnectorArtifact.SDK_REST
import static com.boomi.connector.gradle.ConnectorArtifact.SDK_SAMPLES
import static com.boomi.connector.gradle.ConnectorArtifact.SDK_TEST_UTIL
import static com.boomi.connector.gradle.ConnectorArtifact.SDK_UTIL
import static com.boomi.connector.gradle.ConnectorArtifact.SDK_WS
import static com.boomi.connector.gradle.ConnectorArtifact.exclusions
import static com.boomi.connector.gradle.ConnectorArtifact.values

/**
 * 
 */
class ConnectorArtifactSpec extends Specification {
    
    def "artifact dependency uses correct group and module"() {
        def expected = "$group:$module"
        
        expect:  
        provided.dependency() == expected 
        provided.dependency("") == expected
        
        where: 
        provided        | group                 | module 
        BOOMI_UTIL      | "com.boomi.util"      | "boomi-util"
        BOOMI_JSON_UTIL | "com.boomi.util"      | "boomi-json-util"
        COMMON_LITE     | "com.boomi.util"      | "common-lite"
        COMMON_SDK      | "com.boomi.commonsdk" | "common-sdk"
        SDK_API         | "com.boomi.connsdk"   | "connector-sdk-api"   
        SDK_MODEL       | "com.boomi.connsdk"   | "connector-sdk-model"
        SDK_OPEN_API    | "com.boomi.connsdk"   | "connector-sdk-openapi"
        SDK_REST        | "com.boomi.connsdk"   | "connector-sdk-rest"
        SDK_SAMPLES     | "com.boomi.connsdk"   | "connector-sdk-samples"
        SDK_TEST_UTIL   | "com.boomi.connsdk"   | "connector-sdk-test-util"
        SDK_UTIL        | "com.boomi.connsdk"   | "connector-sdk-util"  
        SDK_WS          | "com.boomi.connsdk"   | "connector-sdk-ws"
        
    }
        
    def "artifact dependency accepts custom version"() {
        expect:
        artifact.dependency(version) == "$artifact.group:$artifact.module:$version"
        
        where:
        artifact << values()
        version << (1..values().size()).collect {it}
    }
    
    def "excluded artifacts use correct group and module"() {
        def included = [BOOMI_UTIL, COMMON_LITE, SDK_UTIL, COMMON_SDK, SDK_MODEL, SDK_WS, SDK_OPEN_API, SDK_REST,
                        SDK_TEST_UTIL, BOOMI_JSON_UTIL]
        def expected = (values() - included).collect {[group: it.group, module: it.module]}
    
        expect:
        exclusions() == expected
    }
}
