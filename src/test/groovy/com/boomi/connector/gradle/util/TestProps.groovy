package com.boomi.connector.gradle.util

/**
 * Helper class to load various test properties
 */
final class TestProps {

    private TestProps() {
    }
    
    /**
     * Get the expected sdk version 
     * 
     * @return the sdk version string 
     */
    public static String expectedSdkVersion() {
        loadGradleProperties().getProperty("sdkVersion")
    }
    
    private static Properties loadGradleProperties() {
        Properties props = new Properties()
        new FileInputStream("gradle.properties").withCloseable { 
            props.load(it)
        }
        props
    }    
    
}