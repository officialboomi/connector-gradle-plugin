package com.boomi.connector.gradle.util

import com.boomi.connector.gradle.util.ResourceUtil
import spock.lang.Specification

class ResourceUtilSpec extends Specification {

    def "get resource returns null for missing resource"() {
        when:
        def resource = ResourceUtil.getResource("doesNotExist")
        
        then:
        !resource
    }
    
    def "get resource returns content stream"() {
        when:
        def resource = ResourceUtil.getResource("test.properties")
        
        then:
        resource.withCloseable {            
            "key=value" == it.text
        }         
    }
    
    def "load properties returns empty instance for missing resource"() {
        when:
        def props = ResourceUtil.loadProperties("nope")
        
        then:
        props.isEmpty()
    }
    
    def "load properties returns properties instance"() {
        when:
        def props = ResourceUtil.loadProperties("test.properties")
        
        then:
        props.getProperty("key") == "value"
    }
}
