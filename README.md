# Boomi Connector Plugin 

Plugin to streamline connector development using gradle.

## Requirements

This Gradle plugin is compatible with Gradle version 7 and version 8.

## Applying the plugin 

```groovy
plugins {
    id 'com.boomi.connector' version '0.5.1'
}
```

### Plugin Versions
All the supported plugin versions are published to the official [Gradle Plugin](https://plugins.gradle.org/plugin/com.boomi.connector) repository.
Visit the [com.boomi.connector plugin](https://plugins.gradle.org/plugin/com.boomi.connector) site for the latest version.

### Connector Extension 

The plugin creates a connector extension that you can use to configure the connector build: 

| Property   | Description                                                                                                                                                                                                                                                                                                                                                                                                |
|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| sdkVersion | (Optional) Specify the connector SDK version. If not provided, a default version is used to resolve SDK dependencies. The version will be included in the generated runtime configuration file. For the latest version, refer to the [Connector SDK changelog](https://help.boomi.com/docs/atomsphere/integration/connectors/r-atm-changelog_for_the_connector_sdk_c2c83f82-b7af-474f-be28-a3480a50e5c9/). |
| className  | (Required) Specify the connector class name, a fully-qualified class that implements com.boomi.connector.api.Connector. The class name will be included in the generated runtime configuration file.                                                                                                                                                                                                       |

```groovy
connector {
    sdkVersion '2.23.2'    
    className 'com.boomi.sample.SampleConnector'
}
```

### Tasks

The plugin creates the following tasks: 

| Task                                | Description                                                            |
|-------------------------------------|------------------------------------------------------------------------|
| compileConnectorHelloWorldJava      | Compiles the connector hello world Java source.                        |
| compileConnectorSamplesJava         | Compiles the connector samples Java source.                            |
| connectorArchive                    | Assembles the connector archive (car) file.                            |
| connectorHelloWorldArchive          | Assembles the hello world connector archive (car) file.                |
| connectorHelloWorldClasses          | Assembles the connector hello world classes.                           |
| connectorHelloWorldJar              | Assembles a JAR archive containing the connector hello world classes.  |
| connectorSamplesClasses             | Assembles the connector samples classes.                               |
| connectorSamples                    | Downloads the connector SDK sample source files.                       |
| connectorConfig                     | Generates the connector runtime configuration file.                    |
| connectorHelloWorldConfig           | Generates the connector hello world runtime configuration file.        |
| connectorHelloWorldSource           | Generates the connector hello world source files.                      |
| processConnectorHelloWorldResources | Processes the connector hello world resources.                         |
| processConnectorSamplesResources    | Processes the connector samples resources.                             |

## Building a connector 

The connectorArchive task is the primary task to build a connector, producing the connector archive (car file) and is a dependency of the `build` task.

By default, the connector archive contains: 

- JAR file produced from the project's `main` source set. 
- Generated connector runtime configuration file with strict classloading opted in. 
- Any resources in the `META-INF` subdirectory of the `main` source set's resource directory. 
- Any declared and implicit connector dependencies

### Declaring dependencies 

Connector dependencies can be declared using the `api`, `implementation`, and `connectorLib` configurations. The appropriate configuration depends on whether the dependency must be used to compile the connector, packaged in the connector archive, or both. 

| Configuration  | Compile? | Package? |
|----------------|----------|----------|
| api            | Y        | Y        |
| implementation | Y        | N        |
| connectorLib   | N        | Y        |

The following Boomi dependency is provided by the Atom runtime and is automatically excluded from the connector archive:

- connector-sdk-api

The plugin implicitly adds a dependency for `implementation "com.boomi.connsdk:connector-sdk-api"` so it does not have to be explicitly declared.
Additionally, an implicit dependency for `api com.boomi.connsdk:connector-sdk-util` is added with a version constraint to package and keep its version consistent with the API.

```groovy
dependencies {
    api "com.boomi.connsdk:connector-sdk-util"
}
```

### Repositories 

The plugin automatically registers the Connector SDK Maven repository. 

| Name     | URL                                        |
|----------|--------------------------------------------|
| boomisdk | https://boomisdk.s3.amazonaws.com/releases |

### Packaging an existing runtime configuration

To package an existing runtime configuration file, the `connectorConfig` task must be disabled by adding `project.tasks.connectorConfig.enabled = false` to the build script, or `-X connectorConfig` to the command line.

### Packaging additional files 

The `connectorArchive` task is a gradle `Zip` task, so all the standard `CopySpec` semantics are supported. 

```groovy
connectorArchive {
    from "path/to/directory", {
        include "fileToPackage"
    }
}

```

### Sample Build 

```groovy
plugins {
    id 'com.boomi.connector' version '0.5.1'
}

connector {
    sdkVersion '2.23.2'    
    className 'com.boomi.sample.SampleConnector'
}

dependencies {
    api "commons-io:commons-io:2.8.0"

    api "com.boomi.util:boomi-util:2.3.8"

    testImplementation "junit:junit:4.12"
}

```
## OpenAPI Connector Extension

The plugin also creates an `openAPIConnector{}` extension that you can use to configure and build an OpenAPI connector. The extension essentially extends all the above functionalities provided by the `connector` extension and modifies the behavior of [tasks](#modified-tasks). 
The extension lets you define connection fields and operations in an easy to understand Groovy DSL configuration based input format. Let's take a look at each of them, starting with Connection fields.

##### Connection Fields
The plugin enables you to define the value of below-mentioned connection fields either as a String, boolean, or a closure,
 depending upon the field itself. A closure definition lets you configure subfields of a particular connection field 
 such as label, helpText, defaultValue etc. These details are used to auto-generate a ready to use connector-descriptor file, 
 and additional runtime configuration files.  

| Property             | Description                                                                                                                                                                                                                                                                  | Input value can be                   |
|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|
| openApiSpecification | (Required) Web endpoint or project resource file location where the OpenAPI specification of the service you are trying to connect to can be found                                                                                                                           | String only                          |
| authType             | (Required) Authentication mechanism for the service. If no authentication is required, please specify "NONE"                                                                                                                                                                 | String <br/> (or) <br/> List_Field   |
| server               | (Optional) Server URL of the hosting service. This URL will be used as the root from which all REST calls will be made, in conjunction with the paths defined in the OpenAPI specification. This field will be generated in the descriptor regardless of its inclusion here. | String only                          |
| cookieScope          | (Optional) The scope of HTTP Cookies                                                                                                                                                                                                                                         | String <br/> (or) <br/> List_Field   |
| includePublicCert    | (Optional) Includes a public cert field if set to true or if set as a closure                                                                                                                                                                                                | boolean <br/> (or) <br/> Label_Field |
| includePrivateCert   | (Optional) Includes a private cert field if set to true or if set as a closure                                                                                                                                                                                               | boolean <br/> (or) <br/> Label_Field |

> **Note:** To reference a local OpenAPI Specification, you can:<br>
> 1. add your specification file under your project's resource directory. e.g. `src/main/resources/path/to/openApiSpecification.json`<br>
> 2. set your `openApiSpecification` with a path relative to your project's resource directory. e.g. `openApiSpecification "path/to/openApiSpecification.json"`

##### OAUTH2 Fields
You can also configure `OAUTH2` connection field values with the `OAuth2Config{}` closure, if `OAUTH2` is one of the types of authentication mechanism supported by the service. 
Below is a list of all the available OAUTH2 config fields supported:
**NOTE: OAuth2 Fields can be visually modified by adding `access "hidden|readOnly|enabled"` to the closure**

| Property                   | Description                                                                                 | Input value can be                    |
|----------------------------|---------------------------------------------------------------------------------------------|---------------------------------------|
| authorizationTokenEndpoint | (Optional) authorization endpoint of the OAuth2 field                                       | String <br/> (or) <br/> Default_Field |
| accessTokenEndpoint        | (Optional)  access endpoint of the OAuth2 field                                             | String <br/> (or) <br/> Default_Field |
| authorizationParameters    | (Optional) additional parameters required when interacting with the authorization endpoint  | Key_Value_Pair_Field                  |
| accessTokenParameters      | (Optional)  additional parameters required when interacting with the access endpoint        | Key_Value_Pair_Field                  |
| scope                      | (Optional) OAuth2 scope                                                                     | String <br/> (or) <br/> Default_Field |
| grantType                  | (Optional) OAuth2 grant type                                                                | String <br/> (or) <br/> List_Field    |

Also, if `jwt-bearer` happens to be a one of the options or the value of `grantType` field from above, you can further configure additional JWT parameters
with the `jwtParameters{}` closure as part of the `OAUTH2Config{}` parent closure. Following fields are available under `jwtParameters{}`.

| Property            | Description                                                           | Input value can be                    |
|---------------------|-----------------------------------------------------------------------|---------------------------------------|
| signatureAlgorithms | (Optional)  Algorithm to use to sign the JWT                          | String <br/> (or) <br/> List_Field    |
| issuer              | (Optional)  Identifies the principal who issued the claim             | String <br/> (or) <br/> Default_Field |
| subject             | (Optional) Identifies the principal that is the subject of the claim  | String <br/> (or) <br/> Default_Field |
| audience            | (Optional)  Identifies who the JWT is intended for                    | String <br/> (or) <br/> Default_Field |
| expiration          | (Optional) The duration during which the JWT will be valid in millis  | String <br/> (or) <br/> Default_Field |
| signatureKey        | (Optional) Used to sign the JWT                                       | String <br/> (or) <br/> Basic_Field   |
| extendedClaims      | (Optional) Public or private claims which must be included in the JWT | Key_Value_Pair_Field                  |

Currently we support the types of closure definitions listed below with different combinations of subfields available under each type. 

```groovy 
Label_Field {
    label "Custom label"
}
List_Field {
    label  "String"
    helpText "String"
    defaultValue "String"
    includedValues "entry1", "entry2", "entry3", ...
}
 Basic_Field {
    helpText "String"
}
Default_Field {
    helpText "String"
    defaultValue "String"
}
Key_Value_Pair_Field {
    keyValuePairs "key1":"value1", "key2":"value2", ...    
}
```

##### Operations

The plugin lets you define operations that you want to support in the connector with the help of fields below within the `operations{}` closure. 
You need to define at-least 1 operation as part of the DSL config. You can also define unique schema definitions under an operation from the spec which will be available during browse.
The structure of the `operations{}` closure needs to look as below. Please note you need to define the operationId under a schema as either a String or as a Closure below. 
If defined as a Closure, it needs to mention a unique combination of method + path values from the OpenAPI service spec.
```groovy
operations {
    "Custom Operation#1 Name" {

           "Custom Schema#1 Name" {
                operationId "operationId from spec"          
            }
            ...
            "Custom Schema#N Name" {
                operationId {
                    method "HTTP MethodName from spec"
                    path "relative path from spec"
                }
            }
    }
    ...
    
    "Custom Operation#N Name" {
        ...    
    }
}
```  
All operations will be internally defined as type `EXECUTE` within the descriptor.  

### Modified Tasks
The plugin modifies the following connector tasks:

| Task                | Description                                                                                             |
|---------------------|---------------------------------------------------------------------------------------------------------|
| connectorDescriptor | Generates connector-descriptor.xml file from groovy DSL input.                                          |
| connectorJsonConfig | Generates openapi-config.json resource file from groovy DSL input. Skipped if not an OpenAPI connector. |

## Building an OpenAPI connector 

Once the required inputs are defined using the `openAPIConnector` extension, the `connectorArchive` task is the primary task to build an OpenAPI connector, producing the connector archive (car file) and is a dependency of the `build` task.
The `connectorArchive` task also depends upon execution of `connectorConfig`, `connectorDescriptor` & `connectorJsonConfig` tasks internally. In other words, the above-mentioned tasks are run before the task that generates the archive itself gets run.
Depending upon if required connection fields and at-least one operation is defined under the `openAPIConnector` extension, `connectorDescriptor` & `connectorJsonConfig` tasks are either run or are SKIPPED in case of a regular connector. 

### Declaring dependencies 

Provided the required connection fields and at-least one operation is defined under the `openAPIConnector` extension, 
the plugin implicitly adds a dependency for `api "com.boomi.connsdk:connector-sdk-openapi"` so it does not have to be explicitly declared.

### Example: OpenAPI DSL Input 
```groovy
openAPIConnector {
    openApiSpecification "https://raw.githubusercontent.com/PagerDuty/api-schema/main/reference/REST/openapiv3.json"
    server "https://api.sandbox.paypal.com"
    cookieScope "GLOBAL" 
    includePublicCert {
        label "Custom Public Cert"
    }
    includePrivateCert false
    
    authType {
        defaultValue "OAUTH2"
        includedValues "OAUTH2", "DIGEST", "NONE", "CUSTOM", "BASIC"
        label "Custom label here"
        helpText "Custom helpText here"
    }
    // Only required if OAUTH2 is one of the auth mechanisms used
    OAuth2Config {
        authorizationTokenEndpoint {
             helpText "Custom helpText here"
             defaultValue "abcxyz"
             access "enabled" 
         }
        accessTokenEndpoint {
            helpText "Custom helpText here"
            defaultValue "abcxyz"
            access "enabled"
        }
        authorizationParameters {
            access "enabled"
            keyValuePairs "parameterName1": "parameterValue1", "parameterName2": "parameterValue1"
        }
        accessTokenParameters {
            access "enabled"
            keyValuePairs "parameterName1": "parameterValue1", "parameterName2": "parameterValue1"
        }
        scope {
            helpText "Custom helpText here"
            defaultValue "abcxyz"
            access "enabled"
        }
        grantType {
            helpText "Custom helpText here"
            defaultValue "code"
            includedValues "jwt-bearer","client_credentials"
        }
        jwtParameters {
            signatureAlgorithms {
                helpText "Custom helpText here"
                defaultValue "NONE"
                includedValues "NONE", "SHA256withRSA"
                access "enabled"
            }
            issuer {
                helpText "Custom helpText here"
                defaultValue "abcxyz"
                access "enabled"
            }
            subject {
                helpText "Custom helpText here"
                defaultValue "abcxyz"
                access "enabled"
            }
            audience {
                helpText "Custom helpText here"
                defaultValue "abcxyz"
                access "enabled"
            }
            expiration {
                helpText "Custom helpText here"
                defaultValue "3600000" // value in ms
                access "enabled"
            }
            signatureKey {
                helpText "Custom helpText here"
                access "enabled"
            }
            extendedClaims {
                access "enabled"
                keyValuePairs "claimName1": "claimValue1", "claimName2": "claimValue2", "claimName3": "claimValue3"
            }
        }
    }
   
    operations {
        "Create" {
            "Install an Add-on" {
                operationId "createAddon"
            }
            "Create a user contact method" {
                operationId "createUserContactMethod"
            }
            "Get aggregated incident data" {
                operationId "getAnalyticsMetricsIncidentsAll"
            }
            "Update an Add-on" {
                operationId {
                    method "PUT"
                    path "/addons/{id}"
                }
            }
        }
        "Get" {
            "List installed Add-ons" {
                operationId "listAddon"
            }
            "List Users"{
                operationId "listUsers"
            }
            "getUserContactMethods" {
                operationId "getUserContactMethods"
            }
            "Get aggregated incident dataBoo" {
                operationId "listAddon"
            }
            "Get an Add-on" {
                operationId {
                    method "GET"
                    path "/addons/{id}"
                }
            }
        }
    }
}
```
## Connector Hello World 

The plugin supports the generation of a "hello world" connector. The `connectorHelloWorldArchive` task and dependent tasks will generate the necessary source files and package them into a connector archive. 

The source files are added to the connectorHelloWorld source set, creating a `generated/src/connectorHelloWorld` source directory in the project's build directory. To package modified versions of the source or runtime configuration file, the `generateConnectorHelloWorldSource` or `generateConnectorHelloWorldConfig` tasks must be disabled. 

## Connector SDK Samples 

The plugin supports downloading the `connector-sdk-samples` project using the `downloadConnectorSamples` task. The source files are added to the `connectorSamples` source set, creating a `generated/src/connectorSamples` source directory in the project's build directory.